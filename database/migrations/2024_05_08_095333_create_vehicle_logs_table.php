<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVehicleLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle_logs', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_vehicle');
            $table->bigInteger('id_booking');
            $table->datetime('log_date');
            $table->bigInteger('mileage_start');
            $table->bigInteger('mileage_end');
            $table->text('note_vehicle')->nullable();
            $table->decimal('fuel_consumption');
            $table->unsignedBigInteger('created_by');
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->timestamps();

            $table->foreign('id_vehicle')->references('id')->on('vehicles');
            $table->foreign('id_booking')->references('id')->on('bookings');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicle_logs');
    }
}
