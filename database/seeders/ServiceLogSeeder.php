<?php

namespace Database\Seeders;

use App\Models\ServiceLog;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class ServiceLogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $currentDate = Carbon::now();
        $futureDate = $currentDate->addDays(3);

        $dt = new ServiceLog();
        $dt->id_vehicle = 1;
        $dt->start_date = $currentDate;
        $dt->end_date = $futureDate;
        $dt->note_service = "Mesin Kembali seperti baru";
        $dt->service_fees = 54000;
        $dt->status = "done";
        $dt->created_by = 1;
        $dt->created_at = $currentDate;
        $dt->save();
    }
}
