<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\Vehicle;
use Illuminate\Database\Seeder;

class VehicleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [
            [
                'name' => 'Xenia 2020 / 01',
                'id_region' => 1,
                'id_type' => 1,
                'ownership' => 'owned',
                'status' => 'available',
                'license_plate' =>  'B 9458 NV',
                'created_by' => User::first()->id,
            ],
            [
                'name' => 'Avanza 2019 / 02',
                'id_region' => 1,
                'id_type' => 1,
                'ownership' => 'leased',
                'status' => 'available',
                'license_plate' =>  'B 3563 WE',
                'created_by' => User::first()->id,
            ],
            [
                'name' => 'Xenia 2022 / 03',
                'id_region' => 2,
                'id_type' => 1,
                'ownership' => 'leased',
                'status' => 'available',
                'license_plate' =>  'B 7646 SA',
                'created_by' => User::first()->id,
            ],
            [
                'name' => 'Avanza 2014 / 04',
                'id_region' => 2,
                'id_type' => 1,
                'ownership' => 'owned',
                'status' => 'available',
                'license_plate' =>  'B 7654 QE',
                'created_by' => User::first()->id,
            ],
            [
                'name' => 'Grandmax 2019 / 05',
                'id_region' => 1,
                'id_type' => 2,
                'ownership' => 'owned',
                'status' => 'available',
                'license_plate' =>  'B 9458 NV',
                'created_by' => User::first()->id,
            ],
            [
                'name' => 'Grandmax 2020 / 06',
                'id_region' => 1,
                'id_type' => 2,
                'ownership' => 'owned',
                'status' => 'available',
                'license_plate' =>  'B 3563 WE',
                'created_by' => User::first()->id,
            ],
            [
                'name' => 'Grandmax 2021 / 07',
                'id_region' => 2,
                'id_type' => 2,
                'ownership' => 'leased',
                'status' => 'available',
                'license_plate' =>  'B 7646 SA',
                'created_by' => User::first()->id,
            ],
            [
                'name' => 'Grandmax 2018 / 08',
                'id_region' => 2,
                'id_type' => 2,
                'ownership' => 'owned',
                'status' => 'available',
                'license_plate' =>  'B 7654 QE',
                'created_by' => User::first()->id,
            ],
        ];
        Vehicle::insert($user);
    }
}
