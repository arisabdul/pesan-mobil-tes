<?php

namespace Database\Seeders;

use App\Models\Booking;
use App\Models\Employee;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class BookingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $currentDate = Carbon::now();
        $futureDate = $currentDate->addDays(3);

        $dt = new Booking();
        $dt->id_employee = Employee::where('role', 'admin')->where('id_region', 1)->first()->id;
        $dt->id_vehicle = 1;
        $dt->start_date = $currentDate;
        $dt->end_date = $futureDate;
        $dt->purpose = "Perjalanan Bisnis Luar Kota";
        $dt->status = "pending";
        $dt->created_by = 2;
        $dt->created_at = $currentDate;
        $dt->save();
    }
}
