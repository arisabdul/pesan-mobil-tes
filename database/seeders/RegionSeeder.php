<?php

namespace Database\Seeders;

use App\Models\Region;
use App\Models\User;
use Illuminate\Database\Seeder;

class RegionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $region = [
            [
                'name' => 'Region 1 Malang',
            ],
            [
                'name' => 'Region 2 Surabaya',
            ],
        ];
        Region::insert($region);
    }
}
