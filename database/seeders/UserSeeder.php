<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [
            [
                'id_employee' => 1,
                'username' => 'approver1',
                'level' =>  'approver',
                'password' =>  Hash::make('123456'),
            ],
            [
                'id_employee' => 2,
                'username' => 'admin1',
                'level' =>  'admin',
                'password' =>  Hash::make('123456'),
            ],
            [
                'id_employee' => 3,
                'username' => 'approver2',
                'level' =>  'approver',
                'password' =>  Hash::make('123456'),
            ],
            [
                'id_employee' => 4,
                'username' => 'admin2',
                'level' =>  'admin',
                'password' =>  Hash::make('123456'),
            ]
        ];
        User::insert($user);
    }
}
