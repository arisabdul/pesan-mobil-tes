<?php

namespace Database\Seeders;

use App\Models\Employee;
use App\Models\User;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        $emplyee = [
            [
                'id_region' => 1,
                'name' => $faker->name,
                'phone_number' => $faker->phoneNumber,
                'address' => $faker->address,
                'role' =>  'approver',
            ],
            [
                'id_region' => 1,
                'name' => $faker->name,
                'phone_number' => $faker->phoneNumber,
                'address' => $faker->address,
                'role' =>  'admin',
            ],
            [
                'id_region' => 1,
                'name' => $faker->name,
                'phone_number' => $faker->phoneNumber,
                'address' => $faker->address,
                'role' =>  'approver',
            ],
            [
                'id_region' => 1,
                'name' => $faker->name,
                'phone_number' => $faker->phoneNumber,
                'address' => $faker->address,
                'role' =>  'admin',
            ],
            [
                'id_region' => 2,
                'name' => $faker->name,
                'phone_number' => $faker->phoneNumber,
                'address' => $faker->address,
                'role' =>  'driver',
            ],
            [
                'id_region' => 1,
                'name' => $faker->name,
                'phone_number' => $faker->phoneNumber,
                'address' => $faker->address,
                'role' =>  'driver',
            ],
            [
                'id_region' => 1,
                'name' => $faker->name,
                'phone_number' => $faker->phoneNumber,
                'address' => $faker->address,
                'role' =>  'driver',
            ],
            [
                'id_region' => 2,
                'name' => $faker->name,
                'phone_number' => $faker->phoneNumber,
                'address' => $faker->address,
                'role' =>  'driver',
            ]
        ];
        Employee::insert($emplyee);
    }
}
