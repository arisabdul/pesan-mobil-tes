<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\VehicleType;
use Illuminate\Database\Seeder;

class VehicleTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $type = [
            [
                'type_name' => 'Angkutan 6 Orang',
                'description' =>  'Mobil untuk Mengangkut 6 Orang',
                'created_by' => User::first()->id,
            ],
            [
                'type_name' => 'Angkutan Barang',
                'description' =>  'Mobil untuk Mengangkut Barang',
                'created_by' => User::first()->id,
            ],
            [
                'type_name' => 'Angkutan 4 Orang',
                'description' =>  'Mobil untuk Mengangkut 4 Orang',
                'created_by' => User::first()->id,
            ],
        ];

        VehicleType::insert($type);
    }
}
