<?php

namespace App\Models;

use App\Traits\CreateByTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    use HasFactory, CreateByTrait;

    const STATUS = [
        'pending' => 'Pending',
        'approved' => 'Disetujui',
        'declined' => 'Ditolak',
        'cancelled' => 'Dibatalkan',
    ];

    public function employee()
    {
        return $this->hasOne(Employee::class, 'id', 'id_employee');
    }

    public function vehicle()
    {
        return $this->hasOne(Vehicle::class, 'id', 'id_vehicle');
    }

    public function pembuat()
    {
        return $this->hasOne(User::class, 'id', 'created_by');
    }

    public function approval()
    {
        return $this->belongsTo(Approval::class, 'id_booking', 'id');
    }

    public function approvalApprover()
    {
        return $this->hasOne(Approval::class, 'id_booking', 'id')->where('approval_level', 'approver');
    }

    public function approvalAdmin()
    {
        return $this->hasOne(Approval::class, 'id_booking', 'id')->where('approval_level', 'admin');
    }
}
