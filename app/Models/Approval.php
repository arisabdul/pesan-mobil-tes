<?php

namespace App\Models;

use App\Traits\CreateByTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Approval extends Model
{
    use HasFactory, CreateByTrait;

    const STATUS = [
        'approver' => [
            'pending' => 'Pending Approver',
            'approved' => 'Disetujui Approver',
            'declined' => 'Ditolak Approver',
        ],
        'admin' => [
            'pending' => 'Pending Admin',
            'approved' => 'Disetujui Admin',
            'declined' => 'Ditolak Admin',
            'cancelled' => 'Dibatalkan Admin',
        ]
    ];
}
