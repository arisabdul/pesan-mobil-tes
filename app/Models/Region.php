<?php

namespace App\Models;

use App\Traits\CreateByTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    use HasFactory, CreateByTrait;

    public function employee()
    {
        return $this->hasMany(Employee::class, 'id_region', 'id');
    }

    public function vehicle()
    {
        return $this->hasMany(Vehicle::class, 'id_region', 'id');
    }
}
