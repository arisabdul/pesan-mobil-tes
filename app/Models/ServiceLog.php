<?php

namespace App\Models;

use App\Traits\CreateByTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServiceLog extends Model
{
    use HasFactory, CreateByTrait;

    const STATUS = [
        'waiting' => 'Menunggu Jadwal',
        'progress' => 'Proses Perbaikan',
        'done' => 'Selesai',
        'canceled' => 'Dibatalkan',
    ];

    public function vehicle()
    {
        return $this->hasOne(Vehicle::class, 'id', 'id_vehicle');
    }


}
