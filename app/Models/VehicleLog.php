<?php

namespace App\Models;

use App\Traits\CreateByTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class VehicleLog extends Model
{
    use HasFactory, CreateByTrait;
    
    protected $casts = [
        'log_date' => 'datetime',
    ];

    protected $appends = [
        'formatted_log_date',
    ];

    public function booking() {
        return $this->hasOne(Booking::class, 'id', 'id_booking');
    }

    public function getFormattedLogDateAttribute() {
        return $this->log_date->format('d, F Y');
    }
}
