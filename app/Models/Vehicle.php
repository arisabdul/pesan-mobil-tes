<?php

namespace App\Models;

use App\Traits\CreateByTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    use HasFactory, CreateByTrait;

    const STATUS = [
        'available' => 'Tersedia',
        'in_use' => 'Dipakai',
        'maintenance' => 'Pemeliharaan',
        'decommissioned' => 'Dinon-aktifkan',
    ];

    const OWNERSHIP = [
        'owned' => 'Milik Perusahaan',
        'leased' => 'Sewa',
    ];

    public function vehicleLog()
    {
        return $this->hasMany(VehicleLog::class, 'id_vehicle', 'id');
    }

    public function booking()
    {
        return $this->hasMany(Booking::class, 'id_vehicle', 'id');
    }

    public function vehicleType()
    {
        return $this->hasOne(VehicleType::class, 'id', 'id_type');
    }

    public function region()
    {
        return $this->hasOne(Region::class, 'id', 'id_region');
    }
}
