<?php

namespace App\Models;

use App\Traits\CreateByTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasFactory, CreateByTrait;

    const ROLE = [
        'approver' => 'approver',
        'admin' => 'admin',
        'driver' => 'driver',
    ];

    const STATUS = [
        'available' => 'Tersedia',
        'driving' => 'Mengendarai',
        'off' => 'Libur / Off',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'id', 'id_employee');
    }
    public function region()
    {
        return $this->hasOne(Region::class, 'id', 'id_region');
    }

    public function booking()
    {
        return $this->hasMany(Booking::class, 'id_employee', 'id');
    }
}
