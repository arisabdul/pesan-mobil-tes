<?php
/**
* @author Nando (c) 2018
* Simple Ajax Trait
*/
namespace App\Traits;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;

trait CreateByTrait
{
    protected static function booted()
    {
        parent::boot();
        static::creating(function ($model) {
            try {
                if (!app()->runningInConsole()) {
                    $model->created_by = auth()->user()->id ?? NULL;
                    $model->created_at = Carbon::now()->toDateTimeString();
                    $model->updated_at = NULL;
                }
            } catch (\Exception $e) {
                Log::error("ERROR By Trait : " . $e->getMessage());
                abort(500, $e->getMessage());
            }
        });

        static::updating(function ($model) {
            try {
                if (!app()->runningInConsole()) {
                    $model->updated_by = auth()->user()->id ?? NULL;
                    $model->updated_at = Carbon::now()->toDateTimeString();
                }
            } catch (\Exception $e) {
                Log::error("ERROR By Trait : " . $e->getMessage());
                abort(500, $e->getMessage());
            }
        });
    }
}
