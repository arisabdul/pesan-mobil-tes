<?php

namespace App\Http\Controllers\RiwayatPakai;

use App\Http\Controllers\Controller;
use App\Models\VehicleLog;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use DB;

class RiwayatPakaiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function datatable(Request $request) {
        $data = VehicleLog::with('booking.employee');
        if (!empty($request->tgl_pakai)) {
            $data = $data->whereDate('log_date', '=', $request->tgl_pakai);
        }

        return Datatables::of($data)
                    ->editColumn('formatted_log_date', function ($vehicleLog) {
                        return $vehicleLog->formatted_log_date;
                    })
                    ->addIndexColumn()
                    ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return view('pages.riwayat.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $vl = VehicleLog::where('id_booking', $id)->first();
        $data['idBooking'] = $id;
        $data['vl'] = $vl;

        return view('pages.riwayat.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        DB::beginTransaction();
        try {
            $vl = VehicleLog::where([
                'id_booking' => $id
            ])->first();
            
            if (!empty($request->mileage_end))
                $vl->mileage_end = $request->mileage_end;
            
            if (!empty($request->fuel_con))
                $vl->fuel_consumption = $request->fuel_con;

            if (!empty($request->description))
                $vl->note_vehicle = $request->description;

            $vl->save();

            DB::commit();
        } catch (\Exception $e) {
            if (env('CATCH_DEBUG'))
                dd($e->getMessage());

            DB::rollBack();
            return redirect()->route('riwayat.edit', $id)->with('error_msg', "Gagal Input Riwayat Pemakaian");

        }
        // return redirect()->route('riwayat.index')->withSuccess('Berhasil Input Riwayat Pemakaian !');
        return redirect()->route('pemesanan.pemesanan-mobil.index')->withSuccess('Berhasil Input Riwayat Pemakaian !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
