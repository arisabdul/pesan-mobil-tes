<?php

namespace App\Http\Controllers\Mobil;

use App\Http\Requests\Mobil\ServiceLogRequest;
use App\Models\ServiceLog;
use App\Models\User;
use App\Models\Vehicle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class ServiceLogController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (!empty(session('error_msg')))
            Alert::error('Gagal !', session('error_msg'));
        if (!empty(session('success')))
            Alert::success('Success !', session('success'));

        return view('pages.mobil.service-log.index');
    }


    public function datatable(Request $request)
    {
        $data = ServiceLog::with('vehicle.vehicleType', 'vehicle.region')
            ->whereHas('vehicle', function($q) use ($request) {
                if (!empty($request->mobil))
                    $q->where('id', $request->mobil);
                if (!empty($request->mobilType))
                    $q->where('id_type', $request->mobilType);
                if (!empty($request->ownership))
                    $q->where('ownership', $request->ownership);
            });

        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('status', function ($row) {
                $status = ServiceLog::STATUS;
                $btn = '';
                if ($row->status == 'done' || $row->status == 'canceled') {
                    return ServiceLog::STATUS[$row->status];
                } else {
                    $btn .= '<select data-id="' . $row->id . '" class="control-form select-status" >';
                    foreach ($status as $key => $value) {
                        $selected = $row->status == $key ? "selected" : "";
                        $btn .= '<option ' . $selected . ' value="' . $key . '">' . $value . '</option>';
                    }
                    $btn .= '</select>';
                }

                return $btn;
            })
            ->addColumn('action', function ($row) {
                $btn = '<div class="d-flex">';
                if ($row->status == 'done') {
                    $btn .= '<button onClick="setLaporanConf('. $row->id .')" class="btn btn-info d-flex">
                                <i class="bi bi-flag-fill me-1"></i>
                                Laporan
                            </button>';
                }
                $btn .= '   <a href="' . route('mobil.service-log.edit', $row->id) . '" class="btn-edit btn btn-warning mx-1 d-flex"><i class="bi bi-pencil-fill me-1"></i>Edit</a>
                            <button onClick="deleteConf(' . $row->id . ')" class="btn btn-danger d-flex btn-delete">
                                <i class="bi bi-trash-fill me-1"></i>
                                Hapus
                            </button>   
                        </div>
                    ';

                return $btn;
            })
            ->rawColumns(['action', 'status'])
            ->make(true);
    }

    public function changeStatus(ServiceLogRequest $request)
    {
        DB::beginTransaction();
        try {
            $dt = ServiceLog::with('vehicle')
                ->findOrFail($request->id);

            if ($dt->vehicle->status == 'in_use') {
                return response()->json([
                    'success' => false,
                    'message' => "Tidak bisa mengganti status ketika mobil sedang digunakan !",
                    'data' => 'Mohon maaf !'
                ], 422);
            }

            $dt->status = $request->status;
            $dt->save();
            
            if ($dt->start_date == Carbon::now()) {
                $dte = Vehicle::find($dt->vehicle->id);
                if ($dt->status == 'progress') {
                    $dte->status = 'maintenance';
                } else {
                    $dte->status = 'available';
                }
                $dte->save();
            }
            activity()->performedOn($dt)->log('Update Status Jadwal Service Log');
            activity()->performedOn($dte)->log('Update Status Mobil Service Log');

            DB::commit();
        } catch (\Exception $e) {
            if (env('CATCH_DEBUG'))
                dd($e->getMessage());

            DB::rollBack();
            return response()->json([
                'success' => false,
                'message' => "Gagal Merubah Status Service Log !",
                'data' => 'Terjadi Kesalahan !'
            ], 500);
        }

        return response()->json([
            'success' => true,
            'message' => 'Berhasil mengubah Status Service Log !',
        ]);
    }

    public function setLaporan(ServiceLogRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $dt = ServiceLog::with('vehicle')
                ->findOrFail($request->id);
            $dt->end_date = $request->end_date;
            $dt->service_fees = $request->service_fees;
            $dt->save();
            activity()->performedOn($dt)->log('Create Laporan Service Log');

            DB::commit();
        } catch (\Exception $e) {
            if (env('CATCH_DEBUG'))
                dd($e->getMessage());

            DB::rollBack();
            return redirect()->route('mobil.service-log.create')->with('error_msg', "Gagal Mengisi Laporan Service Log");

        }
        return redirect()->route('mobil.service-log.index')->withSuccess('Berhasil Mengisi Laporan Service Log !');

    }

    public function create()
    {
        if (!empty(session('error_msg')))
            Alert::error('Gagal !', session('error_msg'));
        if (!empty(session('success')))
            Alert::success('Success !', session('success'));

        return view('pages.mobil.service-log.create');
    }

    public function store(ServiceLogRequest $request)
    {
        DB::beginTransaction();
        try {
            $dt = Vehicle::find($request->id_mobil);
            if ($dt->status == 'in_use' || $dt->status == 'decommissioned') {
                return redirect()->route('mobil.service-log.create')->with('error_msg', "Mobil tidak bisa di jadwalkan service");
            }
            $dte = new ServiceLog();
            $dte->id_vehicle = $request->id_mobil;
            $dte->note_service = $request->note_service;
            $dte->start_date = $request->start_date;
            $dte->status = 'progress';
            $dte->save();
            
            activity()->performedOn($dte)->log('Create Service Log');
            DB::commit();
        } catch (\Exception $e) {
            if (env('CATCH_DEBUG'))
                dd($e->getMessage());

            DB::rollBack();
            return redirect()->route('mobil.service-log.create')->with('error_msg', "Gagal Menambahkan Service Log");
        }
        return redirect()->route('mobil.service-log.index')->withSuccess('Berhasil Menambahkan Service Log !');
    }

    public function edit($id)
    {
        if (!empty(session('error_msg')))
            Alert::error('Gagal !', session('error_msg'));
        if (!empty(session('success')))
            Alert::success('Success !', session('success'));

        $dt = ServiceLog::with('vehicle')->findOrFail($id);
        $data['data'] = $dt;

        return view('pages.mobil.service-log.edit', $data);
    }

    public function show($id)
    {
        try {
            $dt = ServiceLog::with('vehicle.vehicleType', 'vehicle.region')->findOrFail($id);
        } catch (\Exception $e) {
            if (env('CATCH_DEBUG'))
                dd($e->getMessage());

            DB::rollBack();
            return response()->json([
                'success' => false,
                'message' => "Gagal Mengambil Data Service Log !",
                'data' => null,
            ], 500);
        }

        return response()->json([
            'success' => true,
            'message' => 'Berhasil Mengambil Data Service Log !',
            'data' => $dt
        ]);

    }

    public function update(ServiceLogRequest $request, $id)
    {
        DB::beginTransaction();
        try {

            $dte = ServiceLog::find($id);
            $dte->id_vehicle = $request->id_mobil;
            $dte->note_service = $request->note_service;
            $dte->save();

            activity()->performedOn($dte)->log('Update Service Log');

            DB::commit();
        } catch (\Exception $e) {
            if (env('CATCH_DEBUG'))
                dd($e->getMessage());

            DB::rollBack();
            return redirect()->route('mobil.service-log.edit', $dte->id)->with('error_msg', "Gagal Update Service Log");
        }
        return redirect()->route('mobil.service-log.index')->withSuccess('Berhasil Update Service Log !');
    }

    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $dt = ServiceLog::with('vehicle')->findOrFail($id);
            if ($dt->status == 'progress')
                Vehicle::where('id', $dt->vehicle->id)->update(['status' => 'available']);
            $dt->delete();

            activity()->performedOn($dt)->log('Delete Service Log');

            DB::commit();
        } catch (\Exception $e) {
            if (env('CATCH_DEBUG'))
                dd($e->getMessage());

            DB::rollBack();
            return redirect()->route('mobil.service-log.index')->with('error_msg', "Gagal Hapus Service Log");
        }
        return redirect()->route('mobil.service-log.index')->withSuccess('Berhasil Hapus Service Log !');
    }
}
