<?php

namespace App\Http\Controllers\Mobil;

use App\Http\Requests\Mobil\MobilRequest;
use App\Models\Employee;
use App\Models\Mobil;
use App\Models\Vehicle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use App\Models\VehicleType;
use Laravel\Ui\Presets\Vue;

class MobilController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (!empty(session('error_msg')))
            Alert::error('Gagal !', session('error_msg'));
        if (!empty(session('success')))
            Alert::success('Success !', session('success'));

        return view('pages.mobil.mobil.index');
    }


    public function datatable(Request $request)
    {
        $data = Vehicle::with('vehicleType', 'region');

        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('status', function ($row) {
                $status = Vehicle::STATUS;
                unset($status['maintenance'], $status['in_use']);
                $btn = '';
                if($row->status == 'in_use' || $row->status == 'maintenance') {
                    return Vehicle::STATUS[$row->status];
                } else {
                    $btn .= '<select data-id="' . $row->id . '" class="control-form select-status" >';
                    foreach ($status as $key => $value) {
                        $selected = $row->status == $key ? "selected" : "";
                        $btn .= '<option ' . $selected . ' value="' . $key . '">' . $value . '</option>';
                    }
                    $btn .= '</select>';
                }             

                return $btn;
            })
            ->addColumn('action', function ($row) {
                $btn = '
                    <div class="d-flex">
                        <a href="' . route('mobil.mobil.edit', $row->id) . '" class="btn-edit btn btn-warning mx-1 d-flex"><i class="bi bi-pencil-fill me-1"></i>Edit</a>
                        <button onClick="deleteConf(' . $row->id . ')" class="btn btn-danger d-flex btn-delete">
                            <i class="bi bi-trash-fill me-1"></i>
                            Hapus
                        </button>
                    </div>
                ';

                return $btn;
            })
            ->rawColumns(['action', 'status'])
            ->make(true);
    }

    public function changeStatus(MobilRequest $request)
    {
        DB::beginTransaction();
        try {
            $dt = Vehicle::where('id', $request->id)->first();
            if ($dt->status == 'in_use') {
                return response()->json([
                    'success' => false,
                    'message' => "Tidak bisa mengganti status ketika mobil sedang digunakan !",
                    'data' => 'Mohon maaf !'
                ], 422);
            }
            if ($request->status == 'in_use') {
                return response()->json([
                    'success' => false,
                    'message' => "Tidak bisa mengganti status ke Dipakai tanpa booking terlebih dahulu !",
                    'data' => 'Mohon maaf !'
                ], 422);
            }
            $dt->status = $request->status;
            $dt->save();

            DB::commit();
        } catch (\Exception $e) {
            if (env('CATCH_DEBUG'))
                dd($e->getMessage());

            DB::rollBack();
            return response()->json([
                'success' => false,
                'message' => "Gagal Merubah Status Mobil !",
                'data' => 'Terjadi Kesalahan !'
            ], 500);
        }

        return response()->json([
            'success' => true,
            'message' => 'Berhasil mengubah Status Mobil !',
        ]);
    }

    public function create() {
        if (!empty(session('error_msg')))
            Alert::error('Gagal !', session('error_msg'));
        if (!empty(session('success')))
            Alert::success('Success !', session('success'));

        $data['ownership'] = Vehicle::OWNERSHIP;

        return view('pages.mobil.mobil.create', $data);
    }

    public function store(MobilRequest $request) {

        DB::beginTransaction();
        try {
            $dte = new Vehicle();
            $dte->id_type = $request->id_type;
            $dte->id_region = $request->id_region;
            $dte->name = $request->name;
            $dte->ownership = $request->ownership;
            $dte->license_plate = $request->license_plate;
            $dte->status = 'available';
            $dte->save();
            
            DB::commit();
        } catch (\Exception $e) {
            if (env('CATCH_DEBUG'))
                dd($e->getMessage());

            DB::rollBack();
            return redirect()->route('mobil.mobil.create')->with('error_msg', "Gagal Menambahkan Mobil");

        }
        return redirect()->route('mobil.mobil.index')->withSuccess('Berhasil Menambahkan Mobil !');
    }

    public function edit($id) {
        if (!empty(session('error_msg')))
            Alert::error('Gagal !', session('error_msg'));
        if (!empty(session('success')))
            Alert::success('Success !', session('success'));

        $dt = Vehicle::with('vehicleType', 'region')->findOrFail($id);
        $data['data'] = $dt;
        $data['ownership'] = Vehicle::OWNERSHIP;

        return view('pages.mobil.mobil.edit', $data);
    }

    public function update(MobilRequest $request, $id) {

        DB::beginTransaction();
        try {
            $dte = Vehicle::find($id);
            $dte->id_type = $request->id_type;
            $dte->id_region = $request->id_region;
            $dte->name = $request->name;
            $dte->ownership = $request->ownership;
            $dte->license_plate = $request->license_plate;
            $dte->status = 'available';
            $dte->save();

            DB::commit();
        } catch (\Exception $e) {
            if (env('CATCH_DEBUG'))
                dd($e->getMessage());

            DB::rollBack();
            return redirect()->route('mobil.mobil.edit', $dte->id)->with('error_msg', "Gagal Update Mobil");

        }
        return redirect()->route('mobil.mobil.index')->withSuccess('Berhasil Update Mobil !');
    }

    public function destroy($id) {
        DB::beginTransaction();
        try {
            $dt = Vehicle::findOrFail($id);
            $dt->delete();

            DB::commit();
        } catch (\Exception $e) {
            if (env('CATCH_DEBUG'))
                dd($e->getMessage());

            DB::rollBack();
            return redirect()->route('mobil.mobil.index')->with('error_msg', "Gagal Hapus Mobil");

        }
        return redirect()->route('mobil.mobil.index')->withSuccess('Berhasil Hapus Mobil !');
    }
}
