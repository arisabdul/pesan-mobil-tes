<?php

namespace App\Http\Controllers\Mobil;

use App\Http\Requests\UserRequest;
use App\Models\Employee;
use App\Models\User;
use App\Models\Vehicle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;

class MobilLogController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (!empty(session('error_msg')))
            Alert::error('Gagal !', session('error_msg'));
        if (!empty(session('success')))
            Alert::success('Success !', session('success'));

        return view('pages.mobil.mobil-log.index');
    }


    public function datatable(Request $request)
    {
        $data = Vehicle::with('vehicleType', 'region');

        if (!empty($request->mobil))
            $data = $data->where('id', $request->mobil);
        if (!empty($request->mobilType))
            $data = $data->where('id_type', $request->mobilType);
        if (!empty($request->ownership))
            $data = $data->where('ownership', $request->ownership);

        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('status', function ($row) {
                $btn = '';
                $checked = true;
                if (empty($row->status))
                    $checked = false;

                $btn .= '
                            <button onClick="updateStatus(' . $row->id . ')" class="btn btn-info d-flex text-white">
                                <i class="bi bi-info-circle me-2"></i>
                                <span class="text-capitalize ">Status : <b>' . $row->status . '</b></span>
                            </button>
                        ';

                return $btn;
            })
            ->addColumn('action', function ($row) {
                $btn = '
                            <div class="d-flex">
                                <a href="' . route('riwayat.show', $row->id) . '" class="btn-edit btn btn-warning mx-1 d-flex"><i class="bi bi-eye-fill me-1"></i>Riwayat Pemakaian</a>
                            </div>
                        ';

                return $btn;
            })
            ->rawColumns(['action', 'status'])
            ->make(true);
    }
}
