<?php

namespace App\Http\Controllers\Mobil;

use App\Http\Requests\Mobil\MobilTypeRequest;
use App\Models\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use App\Models\Vehicle;
use App\Models\VehicleType;

class MobilTypeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() {
        if (!empty(session('error_msg')))
            Alert::error('Gagal !', session('error_msg'));
        if (!empty(session('success')))
            Alert::success('Success !', session('success'));
        
        return view('pages.mobil.mobil-type.index');
    }

    public function datatable(Request $request) {
        $data = VehicleType::all();

        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function($row){
                $btn = '
                    <div class="d-flex">
                        <a href="'.route('mobil.mobil-type.edit', $row->id).'" class="btn-edit btn btn-warning mx-1 d-flex"><i class="bi bi-pencil-fill me-1"></i>Edit</a>
                        <button onClick="deleteConf('.$row->id.')" class="btn btn-danger d-flex btn-delete">
                            <i class="bi bi-trash-fill me-1"></i>
                            Hapus
                        </button>
                    </div>
                ';

                return $btn;
            })
            ->rawColumns(['action', 'status'])
            ->make(true);
    }


    public function create() {
        if (!empty(session('error_msg')))
            Alert::error('Gagal !', session('error_msg'));
        if (!empty(session('success')))
            Alert::success('Success !', session('success'));

        return view('pages.mobil.mobil-type.create');
    }

    public function store(MobilTypeRequest $request) {
        
        DB::beginTransaction();
        try {
            $dte = new VehicleType();
            $dte->type_name = $request->name;
            $dte->description = $request->description;
            $dte->save();
            activity()->performedOn($dte)->log('Tambah Vehicle Type');

            DB::commit();
        } catch (\Exception $e) {
            if (env('CATCH_DEBUG'))
                dd($e->getMessage());
            
            DB::rollBack();
            return redirect()->route('mobil.mobil-type.create')->with('error_msg', "Gagal Menambahkan Mobil Type");

        }
        return redirect()->route('mobil.mobil-type.index')->withSuccess('Berhasil Menambahkan Mobil Type !');
    }

    public function edit($id) {
        if (!empty(session('error_msg')))
            Alert::error('Gagal !', session('error_msg'));
        if (!empty(session('success')))
            Alert::success('Success !', session('success'));

        $dt = VehicleType::findOrFail($id);
        $data['data'] = $dt;

        return view('pages.mobil.mobil-type.edit', $data);
    }

    public function update(MobilTypeRequest $request, $id) {
        
        DB::beginTransaction();
        try {
            $dte = VehicleType::where('id', $id)->first();
            $dte->type_name = $request->name;
            $dte->description = $request->description;
            $dte->save();
            activity()->performedOn($dte)->log('Update Vehicle Type');

            DB::commit();
        } catch (\Exception $e) {
            if (env('CATCH_DEBUG'))
                dd($e->getMessage());
            
            DB::rollBack();
            return redirect()->route('mobil.mobil-type.edit', $dte->id)->with('error_msg', "Gagal Update Mobil Type");

        }
        return redirect()->route('mobil.mobil-type.index')->withSuccess('Berhasil Update Mobil Type !');
    }

    public function destroy($id) {
        DB::beginTransaction();
        try {
            $dt = VehicleType::findOrFail($id);
            $cekMobil = Vehicle::where('id_type', $id)->first();

            if (!empty($cekMobil)) 
                return redirect()->route('mobil.mobil-type.index')->with('error_msg', "Gagal Hapus Mobil Type karena ada mobil dengan type ini");
            activity()->performedOn($dt)->log('Delete Vehicle Type');
            
            $dt->delete();
            
            DB::commit();
        } catch (\Exception $e) {
            if (env('CATCH_DEBUG'))
                dd($e->getMessage());
            
            DB::rollBack();
            return redirect()->route('mobil.mobil-type.index')->with('error_msg', "Gagal Hapus Mobil Type");

        }
        return redirect()->route('mobil.mobil-type.index')->withSuccess('Berhasil Hapus Mobil Type !');
    }
}
