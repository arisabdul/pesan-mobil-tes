<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\Employee;
use App\Models\Vehicle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data['vehicle'] = Vehicle::withCount('booking')
            ->where('id_region', Auth::user()->employee->id_region)
            ->get();

        $data['booking'] = Booking::with('employee', 'vehicle')->limit(4)->orderBy('created_at', 'desc')->get();

        return view('pages.dashboard.index', $data);
    }
}
