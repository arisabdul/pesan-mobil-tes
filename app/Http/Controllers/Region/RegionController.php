<?php

namespace App\Http\Controllers\Region;

use App\Http\Requests\Region\RegionRequest;
use App\Models\Employee;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use App\Models\Region;

class RegionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (!empty(session('error_msg')))
            Alert::error('Gagal !', session('error_msg'));
        if (!empty(session('success')))
            Alert::success('Success !', session('success'));

        return view('pages.region.region.index');
    }

    public function datatable(Request $request)
    {
        $data = Region::all();

        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function ($row) {
                $btn = '
                    <div class="d-flex">
                        <a href="' . route('region.region.edit', $row->id) . '" class="btn-edit btn btn-warning  mx-1 d-flex"><i class="bi bi-pencil-fill me-1"></i>Edit</a>
                        <button onClick="deleteConf(' . $row->id . ')" class="btn btn-danger d-flex  btn-delete">
                            <i class="bi bi-trash-fill me-1"></i>
                            Hapus
                        </button>
                    </div>
                ';

                return $btn;
            })
            ->rawColumns(['action', 'status'])
            ->make(true);
    }

    public function create()
    {
        if (!empty(session('error_msg')))
            Alert::error('Gagal !', session('error_msg'));
        if (!empty(session('success')))
            Alert::success('Success !', session('success'));

        return view('pages.region.region.create');
    }

    public function store(RegionRequest $request)
    {

        DB::beginTransaction();
        try {
            $dte = new Region();
            $dte->name = $request->name;
            $dte->save();

            activity()->performedOn($dte)->log('Tambah Region');

            DB::commit();
        } catch (\Exception $e) {
            if (env('CATCH_DEBUG'))
                dd($e->getMessage());

            DB::rollBack();
            return redirect()->route('region.region.create')->with('error_msg', "Gagal Menambahkan Region");
        }
        return redirect()->route('region.region.index')->withSuccess('Berhasil Menambahkan Region !');
    }

    public function edit($id)
    {
        if (!empty(session('error_msg')))
            Alert::error('Gagal !', session('error_msg'));
        if (!empty(session('success')))
            Alert::success('Success !', session('success'));

        $dt = Region::findOrFail($id);
        $data['data'] = $dt;

        return view('pages.region.region.edit', $data);
    }

    public function update(RegionRequest $request, $id)
    {

        DB::beginTransaction();
        try {

            $dte = Region::where('id', $id)->first();
            $dte->name = $request->name;
            $dte->save();

            activity()->performedOn($dte)->log('Update Region');

            DB::commit();
        } catch (\Exception $e) {
            if (env('CATCH_DEBUG'))
                dd($e->getMessage());

            DB::rollBack();
            return redirect()->route('region.region.edit', $dte->id)->with('error_msg', "Gagal Update Region");
        }
        return redirect()->route('region.region.index')->withSuccess('Berhasil Update Region !');
    }

    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $dt = Region::with('employee', 'vehicle')->findOrFail($id);
            if (empty($dt->employee))
                return redirect()->route('region.region.index')->with('error_msg', 'Region telah mempunyai Pegawai !');
            if (empty($dt->vehicle))
                return redirect()->route('region.region.index')->with('error_msg', 'Region telah mempunyai Mobil');
            
            $dt->delete();
            activity()->performedOn($dt)->log('Update Region');

            DB::commit();
        } catch (\Exception $e) {
            if (env('CATCH_DEBUG'))
                dd($e->getMessage());

            DB::rollBack();
            return redirect()->route('region.region.index')->with('error_msg', "Gagal Hapus Region");
        }
        return redirect()->route('region.region.index')->withSuccess('Berhasil Hapus Region !');
    }
}
