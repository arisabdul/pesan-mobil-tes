<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use App\Models\Region;
use App\Models\Vehicle;
use App\Models\VehicleType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\DB;

class Select2Controller extends Controller
{
    public function selectRegion(Request $request) {
        $term = trim($request->term);
        $d = Region::select('id', 'name as text');

        if (!empty($term)) {
            $data = $d->where('name', 'ILIKE',  '%' . $term . '%');
        }

        $data = $d->simplePaginate(25);

        $morePages = true;
        if (empty($data->nextPageUrl())) {
            $morePages = false;
        }

        $results = array(
            "results" => $data->items(),
            "pagination" => array(
                "more" => $morePages
            )
        );

        return Response::json($results);
    }

    public function selectMobilType(Request $request) {
        $term = trim($request->term);
        $d = VehicleType::select('id', 'type_name as text');

        if (!empty($term)) {
            $data = $d->where('type_name', 'ILIKE',  '%' . $term . '%');
        }

        $data = $d->simplePaginate(25);

        $morePages = true;
        if (empty($data->nextPageUrl())) {
            $morePages = false;
        }

        $results = array(
            "results" => $data->items(),
            "pagination" => array(
                "more" => $morePages
            )
        );

        return Response::json($results);
    }

    public function selectMobil(Request $request) {
        $term = trim($request->term);
        $d = Vehicle::select('id', DB::raw(" '(' || license_plate || ') ' || name as text"))
            ->where('id_region', Auth::user()->employee->id_region)
            ->where('id_type', $request->type);

        if (!empty($term)) {
            $data = $d->where('name', 'ILIKE',  '%' . $term . '%');
        }

        $data = $d->simplePaginate(25);

        $morePages = true;
        if (empty($data->nextPageUrl())) {
            $morePages = false;
        }

        $results = array(
            "results" => $data->items(),
            "pagination" => array(
                "more" => $morePages
            )
        );

        return Response::json($results);
    }

    public function selectDriver(Request $request) {
        $term = trim($request->term);
        $d = Employee::select('id', 'name as text')
            ->where('id_region', Auth::user()->employee->id_region)
            ->where('role', 'driver');

        if (!empty($term)) {
            $data = $d->where('name', 'ILIKE',  '%' . $term . '%');
        }

        $data = $d->simplePaginate(25);

        $morePages = true;
        if (empty($data->nextPageUrl())) {
            $morePages = false;
        }

        $results = array(
            "results" => $data->items(),
            "pagination" => array(
                "more" => $morePages
            )
        );

        return Response::json($results);
    }
}
