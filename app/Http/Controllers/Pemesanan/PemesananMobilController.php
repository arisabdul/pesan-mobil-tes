<?php

namespace App\Http\Controllers\Pemesanan;

use App\Http\Requests\Pemesanan\PemesananMobilRequest;
use App\Models\Employee;
use App\Models\Mobil;
use App\Models\Booking;
use App\Models\VehicleLog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use App\Models\Approval;
use App\Models\BookingType;
use App\Models\Region;
use Carbon\Carbon;
use Laravel\Ui\Presets\Vue;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\PemesananMobilExport;
class PemesananMobilController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (!empty(session('error_msg')))
            Alert::error('Gagal !', session('error_msg'));
        if (!empty(session('success')))
            Alert::success('Success !', session('success'));

        $data['dataRegion'] = Region::find(Auth::user()->employee->id_region);

        return view('pages.pemesanan.pemesanan-mobil.index', $data);
    }


    public function datatable(Request $request)
    {
        $data = Booking::with('vehicle.vehicleType', 'pembuat.employee', 'employee', 'approvalAdmin',  'approvalApprover')
            ->whereHas('vehicle', function($q) {
                $q->where('id_region', Auth::user()->employee->id_region);
            });

        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('status_booking', function ($row) {
                return Booking::STATUS[$row->status];
            })
            ->addColumn('appr_approver', function ($row) {
                $status = Approval::STATUS['approver'];
                $apprAp = $row->approvalApprover;

                if (Auth::user()->level == 'approver') {
                    $btn = '';
                    if(!empty($apprAp) && $apprAp->status == 'approved') {
                        return '<b class="text-success">'.$status['approved'].'</b>';
                    } else {
                        $btn .= '<select data-id="' . $row->id . '" class="control-form select-status" >';
                        foreach ($status as $key => $value) {
                            $selected = $row->status == $key ? "selected" : "";
                            $btn .= '<option ' . $selected . ' value="' . $key . '">' . $value . '</option>';
                        }
                        $btn .= '</select>';
                    }             
                } else {
                    $btn = '<b class="text-danger">'.$status['pending'].'</b>';
                    if (!empty($apprAp)) 
                        $btn = '<b class="text-success">'.$status[$apprAp->status].'</b>';
                }

                return $btn;
            })
            ->addColumn('appr_admin', function ($row) {
                $status = Approval::STATUS['admin'];
                $apprAp = $row->approvalApprover;
                $apprAd = $row->approvalAdmin;

                if (!empty($apprAp)) {
                    if ($apprAp->status != 'pending' && $apprAp->status != 'declined' ) {
                        if (Auth::user()->level == 'admin') {
                            $btn = '';
                            if(!empty($apprAd) && $apprAd->status == 'approved') {
                                return '<b class="text-success">'.$status['approved'].'</b>';
                            } else {
                                $btn .= '<select data-id="' . $row->id . '" class="control-form select-status" >';
                                foreach ($status as $key => $value) {
                                    $selected = $row->status == $key ? "selected" : "";
                                    $btn .= '<option ' . $selected . ' value="' . $key . '">' . $value . '</option>';
                                }
                                $btn .= '</select>';
                            }             
                        } else {
                            $btn = '<b class="text-danger">'.$status['pending'].'</b>';
                            if (!empty($apprAd)) 
                                $btn = '<b class="text-success">'.$status[$apprAd->status].'</b>';
                        }
                    } else {
                        if ($apprAp->status != 'declined') 
                            $btn = '<b class="text-danger">Ditolak Approver</b>';
                        if ($apprAp->status != 'pending') 
                            $btn = '<b class="text-danger">Belum ada jawaban Approver</b>';
    
                    }
                } else {
                    $btn = '<b class="text-danger">Belum ada jawaban Approver</b>';
                }

                return $btn;
            })
            ->addColumn('action', function ($row) {

                $status = Approval::STATUS['admin'];
                $apprAd = $row->approvalAdmin;

                $btn = '
                    <div class="d-flex">
                        <a href="' . route('pemesanan.pemesanan-mobil.edit', $row->id) . '" class="btn-edit btn btn-warning mx-1 d-flex"><i class="bi bi-pencil-fill me-1"></i>Edit</a>
                        <button onClick="deleteConf(' . $row->id . ')" class="btn btn-danger d-flex btn-delete">
                            <i class="bi bi-trash-fill me-1"></i>
                            Hapus
                        </button>';

                    // approved by admin
                    if (!empty($apprAd)) {
                        $btn .= '
                            <a href="' . route('riwayat.edit', $row->id) . '" class="btn-edit btn btn-warning mx-1 d-flex"><i class="bi bi-plus  me-1"></i>Riwayat Pemakaian</a>
                        ';
                    }

                $btn .= '</div>';

                return $btn;
            })
            ->rawColumns(['action', 'status', 'appr_approver', 'appr_admin'])
            ->make(true);
    }

    public function changeStatus(PemesananMobilRequest $request)
    {
        DB::beginTransaction();
        try {
            $lvl = Auth::user()->employee->role;
            if ($lvl == 'admin') {
                $dt = Approval::where('id_booking', $request->id)
                    ->where('approval_level', 'approver')
                    ->first();
                if (empty($dt)) {
                    return response()->json([
                        'success' => false,
                        'message' => "Belum ada jawaban dari approver !",
                        'data' => 'Mohon maaf !'
                    ], 422);
                } 

                if($dt->status == 'declined') {
                    return response()->json([
                        'success' => false,
                        'message' => "Permohonan ditolak approver !",
                        'data' => 'Mohon maaf !'
                    ], 422);
                }
            }       
            
            
            $dt = Approval::where('id_booking', $request->id)
                ->where('approval_level', $lvl)
                ->first();
                
            if (empty($dt)) {
                $dt = new Approval();
                $dt->id_approver = Auth::user()->employee->id;
                $dt->approval_level = $lvl;
            } else {
                if ($dt->status == 'approved') {
                    return response()->json([
                        'success' => false,
                        'message' => "Tidak bisa mengganti status ketika sudah diapproved !",
                        'data' => 'Mohon maaf !'
                    ], 422);
                }
            }
            $dt->id_booking = $request->id;
            $dt->status = $request->status;
            $dt->approval_date = Carbon::now();
            $dt->save();

            if ($lvl == 'admin') {
                $dt = Booking::where('id', $request->id)
                    ->first();
                $dt->status = $request->status;
                $dt->save();
            }
            activity()->performedOn($dt)->log('Update Status Pemesanan Mobil');

            DB::commit();
        } catch (\Exception $e) {
            if (env('CATCH_DEBUG'))
                dd($e->getMessage());

            DB::rollBack();
            return response()->json([
                'success' => false,
                'message' => "Gagal Merubah Status Mobil !",
                'data' => 'Terjadi Kesalahan !'
            ], 500);
        }

        return response()->json([
            'success' => true,
            'message' => 'Berhasil mengubah Status Mobil !',
        ]);
    }

    public function export()
    {
        return Excel::download(new PemesananMobilExport, 'pemesanan-mobil.xlsx');
    }

    public function create() {
        if (!empty(session('error_msg')))
            Alert::error('Gagal !', session('error_msg'));
        if (!empty(session('success')))
            Alert::success('Success !', session('success'));

        return view('pages.pemesanan.pemesanan-mobil.create');
    }

    public function store(PemesananMobilRequest $request) {
        DB::beginTransaction();
        try {
            $dte = new Booking();
            $dte->id_vehicle = $request->id_mobil;
            $dte->id_employee = $request->id_driver;
            $dte->start_date = $request->tanggal_peminjaman;
            $dte->end_date = $request->tanggal_pengembalian;
            $dte->purpose = $request->purpose;
            $dte->status = 'pending';
            $dte->save();

            $vl = new VehicleLog();
            $vl->id_vehicle = $request->id_mobil;
            $vl->id_booking = $dte->id;
            $vl->log_date = date('Y-m-d H:i:s');
            $vl->mileage_start = $request->mileage_start;
            $vl->mileage_end = 0;
            $vl->note_vehicle = 0;
            $vl->fuel_consumption = 0;
            $vl->save();

            activity()->performedOn($dte)->log('Tambah Pemesanan Mobil');
            
            DB::commit();
        } catch (\Exception $e) {
            if (env('CATCH_DEBUG'))
                dd($e->getMessage());

            DB::rollBack();
            return redirect()->route('pemesanan.pemesanan-mobil.create')->with('error_msg', "Gagal Menambahkan Pemesanan Mobil");

        }
        return redirect()->route('pemesanan.pemesanan-mobil.index')->withSuccess('Berhasil Menambahkan Pemesanan Mobil !');
    }

    public function edit($id) {
        if (!empty(session('error_msg')))
            Alert::error('Gagal !', session('error_msg'));
        if (!empty(session('success')))
            Alert::success('Success !', session('success'));

        $dt = Booking::with('vehicle.vehicleType', 'employee')->findOrFail($id);
        $data['data'] = $dt;

        return view('pages.pemesanan.pemesanan-mobil.edit', $data);
    }

    public function update(PemesananMobilRequest $request, $id) {

        DB::beginTransaction();
        try {
            $dte = Booking::where('id', $id)->first();
            $dte->id_vehicle = $request->id_mobil;
            $dte->id_employee = $request->id_driver;
            $dte->start_date = $request->tanggal_peminjaman;
            $dte->end_date = $request->tanggal_pengembalian;
            $dte->purpose = $request->purpose;
            $dte->save();

            activity()->performedOn($dte)->log('Update Pemesanan Mobil');

            DB::commit();
        } catch (\Exception $e) {
            if (env('CATCH_DEBUG'))
                dd($e->getMessage());

            DB::rollBack();
            return redirect()->route('pemesanan.pemesanan-mobil.edit', $dte->id)->with('error_msg', "Gagal Update Pemesanan Mobil");

        }
        return redirect()->route('pemesanan.pemesanan-mobil.index')->withSuccess('Berhasil Update Pemesanan Mobil !');
    }

    public function destroy($id) {
        DB::beginTransaction();
        try {
            $dt = Booking::findOrFail($id);
            activity()->performedOn($dt)->log('Destroy Pemesanan Mobil');
            $dt->delete();

            DB::commit();
        } catch (\Exception $e) {
            if (env('CATCH_DEBUG'))
                dd($e->getMessage());

            DB::rollBack();
            return redirect()->route('pemesanan.pemesanan-mobil.index')->with('error_msg', "Gagal Hapus Pemesanan Mobil");

        }
        return redirect()->route('pemesanan.pemesanan-mobil.index')->withSuccess('Berhasil Hapus Pemesanan Mobil !');
    }
}
