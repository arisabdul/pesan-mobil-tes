<?php

namespace App\Http\Controllers\Pegawai;

use App\Http\Requests\Pegawai\PegawaiRequest;
use App\Models\Employee;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use App\Models\Booking;
use Spatie\Activitylog\Models\Activity;
class PegawaiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (!empty(session('error_msg')))
            Alert::error('Gagal !', session('error_msg'));
        if (!empty(session('success')))
            Alert::success('Success !', session('success'));

        $data['status'] = Employee::STATUS;

        return view('pages.pegawai.pegawai.index', $data);
    }

    public function datatable(Request $request)
    {
        $data = Employee::with('user', 'region');

        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('status', function ($row) {
                $status = Employee::STATUS;
                unset($status['driving']);
                $btn = '';

                if ($row->role == 'driver') {
                    if($row->status == 'driving') {
                        $btn = Employee::STATUS[$row->status];
                    } else {
                        $btn .= '<select data-id="'.$row->id.'" class="control-form select-status">';
                        foreach ($status as $key => $value) {
                            $selected = $row->status == $key ? "selected" : "";
                            $btn .= '<option '.$selected.' value="'.$key.'">'.$value.'</option>';
                        }
                        $btn .= '</select>';
                    }
                    
                } else {
                    $btn = Employee::STATUS[$row->status];
                }

                return $btn;
            })
            ->addColumn('action', function ($row) {
                $btn = '
                    <div class="d-flex">
                        <a href="' . route('pegawai.pegawai.edit', $row->id) . '" class="btn-edit btn btn-warning  mx-1 d-flex"><i class="bi bi-pencil-fill me-1"></i>Edit</a>
                        <button onClick="deleteConf(' . $row->id . ')" class="btn btn-danger d-flex  btn-delete">
                            <i class="bi bi-trash-fill me-1"></i>
                            Hapus
                        </button>
                    </div>
                ';

                return $btn;
            })
            ->rawColumns(['action', 'status'])
            ->make(true);
    }

    public function changeStatus(PegawaiRequest $request) {
        DB::beginTransaction();
        try {
            $dt = Employee::where('id', $request->id)->first();

            if ($request->status == 'driving') {
                return response()->json([
                    'success' => false,
                    'message' => "Tidak Bisa mengubah status ketika driver menjadi mengemudi !",
                    'data' => 'Mohon maaf !'
                ], 422);
            }
            
            if ($dt->status == 'driving') {
                return response()->json([
                    'success' => false,
                    'message' => "Tidak Bisa mengubah status ketika driver sedang mengemudi !",
                    'data' => 'Mohon maaf !'
                ], 422);
            }

            $dt->status = $request->status;
            $dt->save();

            activity()->performedOn($dt)->log('Change Status User');

            DB::commit();
        } catch (\Exception $e) {
            if (env('CATCH_DEBUG'))
                dd($e->getMessage());

            DB::rollBack();
            return response()->json([
                'success' => false,
                'message' => "Gagal Merubah Status Pegawai !",
                'data' => 'Terjadi Kesalahan !'
            ], 500);
        }

        return response()->json([
            'success' => true,
            'message' => 'Berhasil mengubah Status Pegawai !',
        ]);
    }

    public function create()
    {
        if (!empty(session('error_msg')))
            Alert::error('Gagal !', session('error_msg'));
        if (!empty(session('success')))
            Alert::success('Success !', session('success'));

        return view('pages.pegawai.pegawai.create');
    }

    public function store(PegawaiRequest $request)
    {

        DB::beginTransaction();
        try {
            $dte = new Employee();
            $dte->id_region = $request->id_region;
            $dte->name = $request->name;
            $dte->phone_number = $request->no_telp;
            $dte->address = $request->alamat;
            $dte->name = $request->name;
            $dte->role = $request->jenis_user;
            $dte->status = 'available';
            if ($request->hasFile('foto_profil')) {
                $imagePath = $request->file('foto_profil')->store('images');
                $dte->photo_profile = $imagePath;
            }
            if ($request->hasFile('foto_ktp')) {
                $imagePath = $request->file('foto_ktp')->store('images');
                $dte->photo_idcard = $imagePath;
            }
            $dte->save();

            if ($request->jenis_user != 'driver') {
                $dt = new User;
                $dt->id_employee = $dte->id;
                $dt->username = $request->username;
                $dt->password = Hash::make($request->password);
                $dt->level = $request->jenis_user;
                $dt->save();
            }

            activity()->performedOn($dte)->log('Create Employee');
            activity()->performedOn($dt)->log('Create User');

            DB::commit();
        } catch (\Exception $e) {
            if (env('CATCH_DEBUG'))
                dd($e->getMessage());

            DB::rollBack();
            return redirect()->route('pegawai.pegawai.create')->with('error_msg', "Gagal Menambahkan Pegawai");
        }
        return redirect()->route('pegawai.pegawai.index')->withSuccess('Berhasil Menambahkan Pegawai !');
    }

    public function edit($id)
    {
        if (!empty(session('error_msg')))
            Alert::error('Gagal !', session('error_msg'));
        if (!empty(session('success')))
            Alert::success('Success !', session('success'));

        $dt = Employee::with('user', 'region')->findOrFail($id);
        $data['data'] = $dt;

        return view('pages.pegawai.pegawai.edit', $data);
    }

    public function update(PegawaiRequest $request, $id)
    {

        DB::beginTransaction();
        try {

            $dte = Employee::where('id', $id)->first();
            $dte->id_region = $request->id_region;
            $dte->name = $request->name;
            $dte->phone_number = $request->no_telp;
            $dte->address = $request->alamat;
            $dte->name = $request->name;

            if ($request->hasFile('foto_profil')) {
                if (!empty($dte->photo_profile))
                    Storage::delete($dte->photo_profile);
                $imagePath = $request->file('foto_profil')->store('images');
                $dte->photo_profile = $imagePath;
            }
            if ($request->hasFile('foto_ktp')) {
                if (!empty($dte->photo_idcard))
                    Storage::delete($dte->photo_idcard);
                $imagePath = $request->file('foto_ktp')->store('images');
                $dte->photo_idcard = $imagePath;
            }
            $dte->save();

            if ($dte->role != 'driver') {
                $dt = User::where('id_employee', $dte->id)->first();
                $dt->id_employee = $dte->id;
                $dt->username = $request->username;
                if (!empty($request->password))
                    $dt->password = Hash::make($request->password);
                $dt->save();
            }

            activity()->performedOn($dte)->log('Update Employee');
            activity()->performedOn($dt)->log('Update User');

            DB::commit();
        } catch (\Exception $e) {
            if (env('CATCH_DEBUG'))
                dd($e->getMessage());

            DB::rollBack();
            return redirect()->route('pegawai.pegawai.edit', $dte->id)->with('error_msg', "Gagal Update Pegawai");
        }
        return redirect()->route('pegawai.pegawai.index')->withSuccess('Berhasil Update Pegawai !');
    }

    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $dt = Employee::with('user')->findOrFail($id);
            $cekBooking = Booking::where('id_employee', $dt->id)
                ->orWhere('created_by', $dt->user->id)
                ->first();

            if (!empty($cekBooking)) 
                return redirect()->route('pegawai.pegawai.index')->with('error_msg', "Gagal Hapus Mobil Type karena sudah melakukan aktifitas pemesanan");

            $dte = User::where('id_employee', $dt->id)->delete();
            $dt->delete();
            
            activity()->performedOn($dt)->log('Delete Employee');

            DB::commit();
        } catch (\Exception $e) {
            if (env('CATCH_DEBUG'))
                dd($e->getMessage());

            DB::rollBack();
            return redirect()->route('pegawai.pegawai.index')->with('error_msg', "Gagal Hapus Pegawai");
        }
        return redirect()->route('pegawai.pegawai.index')->withSuccess('Berhasil Hapus Pegawai !');
    }
}
