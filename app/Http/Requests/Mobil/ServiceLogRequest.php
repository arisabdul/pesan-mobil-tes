<?php

namespace App\Http\Requests\Mobil;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class ServiceLogRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];
        $uri = $this->route()->uri;
        switch (true) {
            case str_contains($uri, "change-status"):
                $rules = [
                    'status' => 'required|in:progress,done,canceled',
                    'id' => 'required',
                ];
                break;
            case str_contains($uri, "store"):
            case str_contains($uri, "update"):
                $rules = [
                    'id_vehicle' => 'required|exists:vehicles,id',
                    'start_date' => 'required|datetime',
                    'note_service' => 'nullable',
                    'service_fees' => 'nullable|decimal',
                    'end_date' => 'nullable|datetime',
                ];
                break;
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'name.required' => 'Nama tidak boleh Kosong !',
            'description.required' => 'Deskripsi tidak boleh Kosong !',
        ];
    }
}
