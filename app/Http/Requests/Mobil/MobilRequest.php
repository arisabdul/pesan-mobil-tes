<?php

namespace App\Http\Requests\Mobil;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class MobilRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];
        $uri = $this->route()->uri;
        switch (true) {
            case str_contains($uri, "change-status"):
                $rules = [
                    'status' => 'required|in:available,in_use,maintenance,decommissioned',
                    'id' => 'required',
                ];
                break;
            case str_contains($uri, "store"):
            case str_contains($uri, "update"):
                $rules = [
                    'id_region' => 'required|exists:regions,id',
                    'id_type' => 'required|exists:vehicle_types,id',
                    'name' => 'required',
                    'license_plate' => 'required',
                    'ownership' => 'required|in:owned,leased',
                ];
                break;
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'name.required' => 'Nama tidak boleh Kosong !',
            'description.required' => 'Deskripsi tidak boleh Kosong !',
        ];
    }
}
