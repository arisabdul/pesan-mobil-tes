<?php

namespace App\Http\Requests\Mobil;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class MobilTypeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];
        $uri = $this->route()->uri;
        switch (true) {
            case str_contains($uri, "store"):
            case str_contains($uri, "update"):
                $rules = [
                    'name' => 'required',
                    'description' => 'required',
                ];
                break;
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'name.required' => 'Nama tidak boleh Kosong !',
            'description.required' => 'Deskripsi tidak boleh Kosong !',
        ];
    }
}
