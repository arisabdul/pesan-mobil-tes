<?php

namespace App\Http\Requests\Pegawai;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class PegawaiRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];
        $uri = $this->route()->uri;
        switch (true) {
            case str_contains($uri, "change-status"):
                $rules = [
                    'status' => 'required|in:available,driving,off',
                    'id' => 'required',
                ];
                break;
            case str_contains($uri, "store"):
                $rules = [
                    'id_region' => 'required|exists:regions,id',
                    'jenis_user' => 'required|in:approver,admin,driver',
                    'name' => 'required',
                    'alamat' => 'required',
                    'no_telp' => 'required',
                    'foto_profil' => 'nullable|mimes:png,jpg|max:2000',
                    'foto_ktp' => 'nullable|mimes:png,jpg|max:2000',
                ];
                if ($this->input('jenis_user') == 'admin' || $this->input('jenis_user') == 'approver') {
                    $rules['username'] = [
                            'required',
                            'unique:users,username'
                    ];
                    $rules['password'] = [
                        'required',
                        'min:6'
                    ];
                    $rules['retype_password'] = [
                        'required',
                        'min:6',
                        'same:password',
                    ];
                }
                break;
            case str_contains($uri, "update"):
                $rules = [
                    'id_region' => 'required|exists:regions,id',
                    'name' => 'required',
                    'alamat' => 'required',
                    'no_telp' => 'required',
                    'foto_profil' => 'nullable|mimes:png,jpg|max:2000',
                    'foto_ktp' => 'nullable|mimes:png,jpg|max:2000',
                ];
                $cek = User::where('id_employee', $this->id)->first();
                if (!empty($cek)) {
                    $rules['username'] = [
                        'required',
                        'unique:users,username,' . $this->id
                    ];
                    $rules['password'] = [
                        'nullable',
                        'min:6'
                    ];
                    if ($this->filled('password')) {
                        $rules['retype_password'] = [
                            'required',
                            'min:6',
                            'same:password',
                        ];
                    }
                }
                break;
        }
        
        return $rules;
    }

    public function messages()
    {
        return [
            'id.required' => 'Id Perlu diisi !',
            'status.required' => 'Status Perlu diisi !',
            'status.in' => 'Pemilihan Status tidak sesuai !',
            
            'id_region.required' => 'Region Perlu dipilih !',
            'id_region.exists' => 'Region tidak ada dalam daftar region !',
            'jenis_user.required' => 'Jenis User harus dipilih !',
            'name.required' => 'Nama tidak boleh Kosong !',
            'no_telp.required' => 'Nomer Telepon tidak boleh Kosong !',
            'alamat.required' => 'Alamat tidak boleh Kosong !',
            'foto_profil.image' => 'Pastikan foto profil adalah gambar !',
            'foto_profil.mimes' => 'Format foto profil yang diupload tidak diizinkan !',
            'foto_profil.max' => 'File foto terlalu besar untuk dikompres, coba kembali dengan ukuran yang lebih kecil',
            'foto_ktp.image' => 'Pastikan foto Ktp adalah gambar !',
            'foto_ktp.mimes' => 'Format foto Ktp yang diupload tidak diizinkan !',
            'foto_ktp.max' => 'File foto terlalu besar untuk dikompres, coba kembali dengan ukuran yang lebih kecil',
            
            'username.required' => 'Username tidak boleh Kosong !',
            'username.unique' => 'Username Sudah Terdaftar !',
            'password.required' => 'Password tidak boleh Kosong !',
            'password.min' => 'Password Minimal 6 karakter !',
            'retype_password.required' => 'Re Type Password tidak boleh Kosong !',
            'retype_password.min' => 'Password Minimal 6 karakter !',
            'retype_password.same' => 'Retype Password Harus sama dengan Password !',
        ];
    }
}
