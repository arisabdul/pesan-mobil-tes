<?php

namespace App\Http\Requests\Pemesanan;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class PemesananMobilRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];
        $uri = $this->route()->uri;
        switch (true) {
            case str_contains($uri, "change-status"):
                $rules = [
                    'status' => 'required|in:pending,approved,declined,cancelled',
                    'id' => 'required',
                ];
                break;
            case str_contains($uri, "store"):
            case str_contains($uri, "update"):
                $rules = [
                    'id_mobil' => 'required|exists:vehicles,id',
                    'id_type' => 'required|exists:vehicle_types,id',
                    'id_driver' => 'required|exists:drivers,id',
                    'purpose' => 'required',
                    'tanggal_peminjaman' => 'required|date',
                    'tanggal_pengembalian' => 'required|date',
                ];
                break;
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'name.required' => 'Nama tidak boleh Kosong !',
            'description.required' => 'Deskripsi tidak boleh Kosong !',
        ];
    }
}
