<?php

namespace App\Exports;

use App\Models\Booking;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class PemesananMobilExport implements FromView
{
    function __construct() {

    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        $model = Booking::with('employee', 'vehicle', 'approvalApprover', 'approvalAdmin')->get();

        $data = [
            'booking' => $model,
        ];

        return view('pages.pemesanan.pemesanan-mobil.export', $data);
    }
}
