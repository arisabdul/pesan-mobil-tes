@guest
@else
    <nav class="navbar navbar-expand-md navbar-light bg-dark shadow-sm text-light">
        <div class="container">
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto align-items-md-center d-flex">
                    <li class="nav-item">
                        <a href="{{ route('dashboard') }}" class="nav-link  text-white me-md-2 me-lg-3 @yield('activeMenuDashboard')">
                            Dashboard
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('pemesanan.pemesanan-mobil.index') }}" class="nav-link text-white me-md-2 me-lg-3 @yield('activeMenuPemesananMobil')">
                            Pemesanan Mobil
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('region.region.index') }}" class="nav-link text-white me-md-2 me-lg-3 @yield('activeMenuRegion')">
                            Region
                        </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a id="mobilNavDrowpdown" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true"  aria-expanded="false" v-pre
                        class="nav-link dropdown-toggle text-white me-md-2 me-lg-3 @yield('activeMenuMobil')"">
                            Mobil
                        </a>
                        <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="mobilNavDrowpdown">
                            <li><a class="dropdown-item @yield('activeSubMenuMobiil')" href="{{ route('mobil.mobil.index') }}">List Mobil</a></li>
                            <li><a class="dropdown-item @yield('activeSubMenuMobiilType')" href="{{ route('mobil.mobil-type.index') }}">Tipe Mobil</a></li>
                            <li><a class="dropdown-item @yield('activeSubMenuMobilLog')" href="{{ route('mobil.mobil-log.index') }}">Mobil Log</a></li>
                            <li><a class="dropdown-item @yield('activeSubMenuServiceLog')" href="{{ route('mobil.service-log.index') }}">Service Log</a></li>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('pegawai.pegawai.index') }}" class="nav-link text-white me-md-2 me-lg-3 @yield('activeMenuPegawai')">
                            Pegawai
                        </a>
                    </li>
                </ul>
                <ul class="navbar-nav ms-auto ">
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle text-white align-items-center d-flex"
                            href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false" v-pre>
                            <i class="bi bi-person-circle me-2"></i>

                            {{ Auth::user()->employee->name }}
                        </a>

                        <div class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                            <p class="dropdown-item mb-0">Username : {{ Auth::user()->username }}</p>
                            <p class="dropdown-item mb-0">Level : {{ Auth::user()->isApprover() ? 'Appo' : 'Admin' }}</p>
                            <a class="dropdown-item bg-danger text-white" href="{{ route('logout') }}"
                                onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <i class="bi bi-box-arrow-right me-2"></i>
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
@endguest
