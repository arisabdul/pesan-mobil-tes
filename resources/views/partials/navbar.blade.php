<nav class="navbar navbar-expand-md navbar-light bg-light  shadow-sm">
    <div class="container">
        <a class="navbar-brand mx-md-auto" href="{{ url('/') }}">
            <img src="{{ asset('img/logo.png') }}" alt="" width="150">
        </a>
        <button class="navbar-toggler ms-auto bg-light" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>
    </div>
</nav>
