@extends('layouts.app')
@section('title', 'List Pegawai')
@section('activeMenuPegawai', 'active')
@section('activeSubMenuPegawai', 'active')

@section('content')
    <div class="container">
        <div class="row justify-content-center">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-body d-flex text-white align-items-center bg-dark p-2">
                        <div class="me-auto ms-2">
                            <h5 class="mb-0">List Pegawai</h5>
                        </div>
                        <div class="ms-auto me-2">
                            <a href="{{ route('pegawai.pegawai.create') }}" class="btn btn-primary"><i
                                    class="bi bi-plus-circle me-2"></i>Tambah Pegawai</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered data-table">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>No Telp</th>
                                        <th>Alamat</th>
                                        <th>Username</th>
                                        <th>Level</th>
                                        <th>Region</th>
                                        <th>Status</th>
                                        <th width="100px">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('after-script')
    <script>
        $(document).ready(function() {
            var table = $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                bDestroy: true,
                ajax: "{{ route('pegawai.pegawai.datatable') }}",
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'name',
                        name: 'name'
                    },
                    {
                        data: 'phone_number',
                        name: 'phone_number'
                    },
                    {
                        data: 'address',
                        name: 'address'
                    },
                    {
                        data: 'user.username',
                        name: 'user.username',
                        render: function(data, type, row) {
                            let username = data;
                            if (data == undefined)
                                username = '-';

                            return `<span class="text-lowercase">${username}</span>`
                        }
                    },
                    {
                        data: 'user.level',
                        name: 'user.level',
                        render: function(data, type, row) {
                            let level = data;
                            if (data == undefined)
                                level = 'driver';

                            return `<b class="text-uppercase">${level}</b>`
                        }
                    },
                    {
                        data: 'region.name',
                        name: 'region.name'
                    },
                    {
                        data: 'status',
                        name: 'status',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });

            $(document).on('change', '.select-status', function(e) {
                e.preventDefault();

                let id = $(this).data('id');
                let status = $(this).val();
                let token = $("meta[name='csrf-token']").attr("content");;

                $.ajax({
                    url: "{{ route('pegawai.pegawai.change-status') }}",
                    method: 'POST',
                    data: {
                        id: id,
                        "_token": token,
                        status: status
                    },
                    beforeSend: function() {
                        swal.fire({
                            title: 'Sedang mengirim ke server, mohon tunggu...',
                            allowEscapeKey: false,
                            allowOutsideClick: false,
                            didOpen: () => {
                                swal.showLoading();
                            }
                        })
                    },
                    success: function(response) {
                        Swal.fire({
                            title: "Berhasil",
                            text: response.message,
                            icon: "success"
                        });
                        table.draw();
                    },
                    error: function(err) {
                        if (err.status == 422) {
                            txtErr = JSON.parse(err.responseText);
                            swal.fire('Gagal', txtErr.message, 'error')
                        } else {
                            swal.fire('Gagal', err.statusText, 'error')
                        }
                        table.draw();
                    },
                });
            })
        })


        function deleteConf(id) {
            var url = "{{ route('pegawai.pegawai.destroy', ':id') }}"
            url = url.replace(':id', id)

            Swal.fire({
                icon: 'warning',
                title: 'Apa kamu yakin?',
                text: "Anda tidak akan dapat mengembalikan data ini kembali!",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, hapus!'
            }).then((result) => {
                if (result.value) {
                    window.location.replace(url);
                }
            });
        };
    </script>
@endpush
