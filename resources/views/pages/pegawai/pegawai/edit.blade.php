@extends('layouts.app')
@section('title', 'Tambah Pegawai')
@section('activeMenuPegawai', 'active')
@section('activeSubMenuPegawai', 'active')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header bg-dark text-white">
                        <div class="me-auto">
                            Edit Pegawai
                        </div>
                    </div>


                    <div class="card-body">
                        <form action="{{ route('pegawai.pegawai.update', $data->id) }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="col-md-7 offset-md-3 my-4">
                                <h5>Data Pribadi</h5>
                            </div>

                            <div class="row mb-3">
                                <label for="id_region" class="col-md-4 col-form-label text-md-end">Region</label>

                                <div class="col-md-6">
                                    <select name="id_region" class="form-control" id="selectRegion" autofocus>

                                    </select>

                                    @error('id_region')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="jenis_user" class="col-md-4 col-form-label text-md-end">Jenis Pegawai</label>

                                <div class="col-md-6">
                                    <select name="jenis_user" class="form-control select2" disabled>
                                        <option value="" disabled selected>Pilih Jenis Pegawai</option>
                                        @if(Auth::user()->isApprover())
                                        <option value="approver" @if ($data->level == 'approver') selected @endif>Approver
                                        </option>
                                        @endif
                                        <option value="admin" @if ($data->level == 'admin') selected @endif>Admin
                                        </option>
                                        <option value="driver" @if ($data->role == 'driver') selected @endif>Driver
                                        </option>
                                    </select>

                                    @error('jenis_user')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="name" class="col-md-4 col-form-label text-md-end">Nama</label>

                                <div class="col-md-6">
                                    <input id="name" type="text"
                                        class="form-control @error('name') is-invalid @enderror" name="name"
                                        value="{{ $data->name }}" required autocomplete="name" autofocus>

                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="no_telp" class="col-md-4 col-form-label text-md-end">No Telepon</label>

                                <div class="col-md-6">
                                    <input id="no_telp" type="text" placeholder="Masukkan Nomor Telepon"
                                        class="form-control @error('no_telp') is-invalid @enderror" name="no_telp"
                                        value="{{ $data->phone_number }}" required autocomplete="no_telp" autofocus>

                                    @error('no_telp')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="alamat" class="col-md-4 col-form-label text-md-end">Alamat</label>

                                <div class="col-md-6">
                                    <textarea name="alamat" placeholder="Masukkan Alamat Domisili" id="alamat" cols="30" rows="10"
                                        class="form-control  @error('alamat') is-invalid @enderror" required autocomplete="alamat" autofocus>{{ $data->address }}</textarea>

                                    @error('alamat')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="foto_profil" class="col-md-4 col-form-label text-md-end">Foto Profil</label>

                                <div class="col-md-6">
                                    <input id="foto_profil" type="file"
                                        class="form-control @error('foto_profil') is-invalid @enderror" name="foto_profil"
                                        value="{{ old('foto_profil') }}" autofocus>

                                    @error('foto_profil')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="foto_ktp" class="col-md-4 col-form-label text-md-end">Foto Ktp</label>

                                <div class="col-md-6">
                                    <input id="foto_ktp" type="file"
                                        class="form-control @error('foto_ktp') is-invalid @enderror" name="foto_ktp"
                                        value="{{ old('foto_ktp') }}" autofocus>

                                    @error('foto_ktp')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <hr>
                            <div class="col-md-7 offset-md-3 my-4">
                                <h5>Data Kredensial Login</h5>
                                <p class="text-danger"><i><b>Note :</b> </i>Hanya Jenis Pegawai Approver dan Admin yang wajib
                                    mengisi Form Data Kredensial Login</p>
                            </div>
                            <div class="row mb-3">
                                <label for="username" class="col-md-4 col-form-label text-md-end">Pegawainame</label>

                                <div class="col-md-6">
                                    <input id="username" type="text" placeholder="Masukkan Pegawainame Credential"
                                        disabled="true"
                                        class="form-control input-cred @error('username') is-invalid @enderror"
                                        name="username" value="{{ $data->user->username ?? null }}"
                                        autocomplete="username" autofocus>

                                    @error('username')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="password" class="col-md-4 col-form-label text-md-end">Password</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" placeholder="Masukkan Password Credential"
                                        disabled="true"
                                        class="form-control input-cred @error('password') is-invalid @enderror"
                                        name="password" autocomplete="password" autofocus>

                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="retype_password" class="col-md-4 col-form-label text-md-end">Re Type
                                    Password</label>

                                <div class="col-md-6">
                                    <input id="retype_password" type="password"
                                        placeholder="Masukkan lagi Password yang sama" disabled="true"
                                        class="form-control input-cred @error('retype_password') is-invalid @enderror"
                                        name="retype_password" autocomplete="retype_password" autofocus>

                                    @error('retype_password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-md-6 offset-md-4 d-flex justify-content-end">
                                    <input type="submit" value="Update" class="btn btn-info  pull-right">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('after-script')
    <script>
        $(document).ready(function() {


            var jenis = '{{ $data->role }}';
            checkPegawaiLevel(jenis);

            function checkPegawaiLevel(jenis) {
                if (jenis === 'approver' || jenis === 'admin' || jenis === 'superior') {
                    $('.input-cred').attr('disabled', false);
                    $('.input-cred').attr('required', true);
                } else {
                    $('.input-cred').attr('disabled', true);
                    $('.input-cred').attr('required', false);
                }
            }

            $('.input-jenis-user').on('change', function(e) {
                jenis = $(this).val();
                checkPegawaiLevel(jenis);
            })

            $('#selectRegion').select2({
                placeholder: 'Pilih Region',
                theme: 'bootstrap-5',
                width: '100%',
                // allowClear: true,
                ajax: {
                    url: "{{ route('select2.select-region') }}",
                    dataType: 'json',
                    delay: 250,
                    data: function(params) {
                        return {
                            term: params.term || '',
                            page: params.page || 1,
                        }
                    },
                    cache: true
                }
            }).on('select2:select', (e) => {

            });

            var selOptionRegion = $("<option selected='selected'></option>").val("{{ $data->region->id }}").text("{{ $data->region->nama }}")
            $('#selectRegion').append(selOptionRegion).trigger('change.select2');
        })
    </script>
@endpush
