@extends('layouts.app')
@section('title', 'Input Riwayat Pemakaian')
@section('activeMenuRegion', 'active')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header bg-dark text-white">
                        <div class="me-auto">
                            Input Riwayat Pemakaian
                        </div>
                    </div>

                    <div class="card-body">
                        <form action="{{ route('riwayat.update', $idBooking) }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="col-md-7 offset-md-3 my-4">
                                <h5>Input Riwayat Pemakaian</h5>
                            </div>

                            <div class="row mb-3">
                                <label for="name" class="col-md-4 col-form-label text-md-end">
                                    Jarak Tempuh Akhir
                                </label>

                                <div class="col-md-6">
                                    <input id="mileage_end" type="number" placeholder="Masukkan Jarak Tempuh Akhir (dalam Kilometer)"
                                        class="form-control @error('mileage_end') is-invalid @enderror" name="mileage_end"
                                        value="{{ $vl->mileage_end }}" required autocomplete="name" autofocus>

                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                    <sub>*Isikan jarak tempuh akhir, setelah kendaraan digunakan</sub>
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="name" class="col-md-4 col-form-label text-md-end">
                                    Konsumsi Bahan Bakar
                                </label>

                                <div class="col-md-6">
                                    <input id="fuel_con" type="number" placeholder="Masukkan Jumlah Konsumsi Bahan Bakar (dalam Liter)"
                                        class="form-control @error('fuel_con') is-invalid @enderror" name="fuel_con"
                                        value="{{ $vl->fuel_consumption }}" required autocomplete="fuel_con" autofocus >

                                    @error('fuel_con')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="name" class="col-md-4 col-form-label text-md-end">
                                    Catatan
                                </label>

                                <div class="col-md-6">
                                    <textarea id="description" type="text" placeholder="Masukkan Catatan (Opsional)"
                                        class="form-control @error('description') is-invalid @enderror" name="description"
                                        required autocomplete="description" autofocus>{{ old('description') }}{{ $vl->note_vehicle }}</textarea>

                                    @error('fuel_con')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                <div class="col-md-6 offset-md-4 d-flex justify-content-end">
                                    <input type="submit" value="Simpan" class="btn btn-primary pull-right">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('after-script')
    <script>
        
    </script>
@endpush
