@extends('layouts.app')
@section('title', 'Riwayat Pemakaian')
@section('activeMenuPemesananMobil', 'active')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="w-100 d-md-flex  text-white align-items-center bg-dark p-2">
                            <div class="me-auto ms-2">
                                <p class="mb-0">Riwayat Pemakaian {{ $dataRegion->name ?? '-' }}</p>
                            </div>
                        </div>
                        <div class="row my-4">
                            <h5 class="mb-2">Filter Table</h5>
                            <div class="form-group col-md-3 col-sm-12">
                                <label>Tgl Pemakaian</label>
                                <input type="date" class="form-control tgl-pakai" required name="tgl_pakai">
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered data-table">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Tgl Pemakaian</th>
                                        <th>Nama Pemakai</th>
                                        <th>Jarak Tempuh Awal</th>
                                        <th>Jarak Tempuh Akhir</th>
                                        <th>Total Jarak</th>
                                        <th>Konsumsi Bahan Bakar</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('after-script')
    <script>
        $(document).ready(function() {
            var table = $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                bDestroy: true,
                ajax: {
                    tupe: "GET",
                    url: "{{ route('riwayat.datatable') }}",
                    data: function(d) {
                        d.tgl_pakai = $('.tgl-pakai').val();
                    },
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'formatted_log_date',
                        name: 'formatted_log_date',
                        searchable: false
                    },
                    {
                        data: 'booking.employee.name',
                        name: 'booking.employee.name'
                    },
                    {
                        data: null,
                        name: 'mileage_start',
                        render: function (data, type) {
                            return `${data.mileage_start} KM`
                        },
                        searchable: false
                    },
                    {
                        data: null,
                        name: 'mileage_end',
                        render: function (data, type) {
                            return `${data.mileage_end} KM`
                        },
                    },
                    {
                        data: null,
                        name: 'mileage_start',
                        render: function (data, type) {
                            return data.mileage_start ?? 0 + data.mileage_end ?? 0 + ' KM'
                        },
                    },
                    {
                        data: null,
                        name: 'fuel_consumption',
                        render: function (data, type) {
                            return data.fuel_consumption + ' Liter'
                        },
                    }
                ]
            });

            $('.tgl-pakai').change(function() {
                table.draw();
            })

        })
    </script>
@endpush
