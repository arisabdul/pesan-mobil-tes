@extends('layouts.app')
@section('title', 'Tambah Mobil Type')
@section('activeMenuMobil', 'active')
@section('activeSubMenuMobilType', 'active')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header bg-dark text-white">
                        <div class="me-auto">
                            Tambah Mobil Type
                        </div>
                    </div>

                    <div class="card-body">
                        <form action="{{ route('mobil.mobil-type.store') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="col-md-7 offset-md-3 my-4">
                                <h5>Data Mobil Type</h5>
                            </div>

                            <div class="row mb-3">
                                <label for="name" class="col-md-4 col-form-label text-md-end">Nama Mobil Type</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" placeholder="Masukkan Nama Lengkap"
                                        class="form-control @error('name') is-invalid @enderror" name="name"
                                        value="{{ old('name') }}" required autocomplete="name" autofocus>

                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="description" class="col-md-4 col-form-label text-md-end">Deskripsi Mobil Type</label>

                                <div class="col-md-6">
                                    <textarea id="description" type="text" placeholder="Masukkan Nama Lengkap"
                                        class="form-control @error('description') is-invalid @enderror" name="description"
                                         required autocomplete="description" autofocus>{{ old('description') }}</textarea>

                                    @error('description')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                <div class="col-md-6 offset-md-4 d-flex justify-content-end">
                                    <input type="submit" value="Simpan" class="btn btn-primary pull-right">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('after-script')
    <script>
        
    </script>
@endpush
