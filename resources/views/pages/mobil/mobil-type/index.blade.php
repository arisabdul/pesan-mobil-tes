@extends('layouts.app')
@section('title', 'Daftar Mobil Type')
@section('activeMenuMobil', 'active')
@section('activeSubMenuMobilType', 'active')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header bg-light d-flex justify-content-start align-items-center text-white">
                        <ul class="nav nav-pills nav-fill">
                            <li class="nav-item">
                                <a class="nav-link @yield('activeSubMenuMobil')" aria-current="page" href="{{ route('mobil.mobil.index') }}">List Mobil Type</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link @yield('activeSubMenuMobilType')" href="{{ route('mobil.mobil-type.index') }}">Mobil Type</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link @yield('activeSubMenuMobilLog')" href="{{ route('mobil.mobil-log.index') }}">Riwayat Pemakaian Mobil</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link @yield('activeSubMenuServiceLog')" href="{{ route('mobil.service-log.index') }}">Service Log</a>
                            </li>
                        </ul>
                    </div>

                    <div class="card-body">
                        <div class="w-100 d-flex text-white align-items-center bg-dark p-2">
                            <div class="me-auto ms-2">
                                <h5 class="mb-0">List Mobil Type</h5>
                            </div>
                            <div class="ms-auto me-2">
                                <a href="{{ route('mobil.mobil-type.create') }}" class="btn btn-primary"><i class="bi bi-plus-circle me-2"></i>Tambah Mobil Type</a>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered data-table">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Deskripsi</th>
                                        <th width="100px">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('after-script')
    <script>
        $(document).ready(function() {
            var table = $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                bDestroy: true,
                ajax: "{{ route('mobil.mobil-type.datatable') }}",
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'type_name',
                        name: 'type_name'
                    },
                    {
                        data: 'description',
                        name: 'description'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });


        })
        function deleteConf(id) {
            var url = "{{ route('mobil.mobil-type.destroy', ':id') }}"
            url = url.replace(':id', id)

            Swal.fire({
                icon: 'warning',
                title: 'Apa kamu yakin?',
                text: "Anda tidak akan dapat mengembalikan data ini kembali!",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, hapus!'
            }).then((result) => {
                if (result.value) {
                    window.location.replace(url);
                }
            });
        };
    </script>
@endpush
