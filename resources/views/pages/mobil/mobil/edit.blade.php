@extends('layouts.app')
@section('title', 'Edit Mobil')
@section('activeMenuMobil', 'active')
@section('activeSubMenuMobil', 'active')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header bg-dark text-white">
                        <div class="me-auto">
                            Tambah Mobil
                        </div>
                    </div>

                    <div class="card-body">
                        <form action="{{ route('mobil.mobil.update', $data->id) }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')

                            <div class="col-md-7 offset-md-3 my-4">
                                <h5>Data Mobil</h5>
                            </div>

                            <div class="row mb-3">
                                <label for="id_type" class="col-md-4 col-form-label text-md-end">Mobil Type</label>

                                <div class="col-md-6">
                                    <select name="id_type" class="form-control" id="selectMobilType" autofocus>

                                    </select>

                                    @error('id_type')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="id_region" class="col-md-4 col-form-label text-md-end">Region</label>

                                <div class="col-md-6">
                                    <select name="id_region" class="form-control" id="selectRegion" autofocus>

                                    </select>

                                    @error('id_region')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="name" class="col-md-4 col-form-label text-md-end">Nama Mobil</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" placeholder="Masukkan Nama Mobil"
                                        class="form-control @error('name') is-invalid @enderror" name="name"
                                        value="{{ $data->name }}" required autocomplete="name" autofocus>

                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            
                            <div class="row mb-3">
                                <label for="license_plate" class="col-md-4 col-form-label text-md-end">Plat Nomor</label>

                                <div class="col-md-6">
                                    <input id="license_plate" type="text" placeholder="Masukkan Nomor Plat Mobil"
                                        class="form-control @error('license_plate') is-invalid @enderror" name="license_plate"
                                        value="{{ $data->license_plate }}" required autocomplete="license_plate" autofocus>

                                    @error('license_plate')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="ownership" class="col-md-4 col-form-label text-md-end">Ownership</label>

                                <div class="col-md-6">
                                    <select name="ownership" class="form-control" autofocus required>
                                        <option value="" disabled selected >Pilih Ownership</option>
                                        @foreach ($ownership as $key => $value)
                                            <option value="{{ $key }}" @if($key == $data->ownership) selected @endif >{{$value}}</option>
                                        @endforeach
                                    </select>

                                    @error('ownership')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                <div class="col-md-6 offset-md-4 d-flex justify-content-end">
                                    <input type="submit" value="Edit" class="btn btn-primary pull-right">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('after-script')
    <script>
        $(document).ready(function() {
            $('#selectMobilType').select2({
                placeholder: 'Pilih Mobil Type',
                theme: 'bootstrap-5',
                width: '100%',
                // allowClear: true,
                ajax: {
                    url: "{{ route('select2.select-mobil-type') }}",
                    dataType: 'json',
                    delay: 250,
                    data: function(params) {
                        return {
                            term: params.term || '',
                            page: params.page || 1,
                        }
                    },
                    cache: true
                }
            }).on('select2:select', (e) => {

            });

            $('#selectRegion').select2({
                placeholder: 'Pilih Region',
                theme: 'bootstrap-5',
                width: '100%',
                // allowClear: true,
                ajax: {
                    url: "{{ route('select2.select-region') }}",
                    dataType: 'json',
                    delay: 250,
                    data: function(params) {
                        return {
                            term: params.term || '',
                            page: params.page || 1,
                        }
                    },
                    cache: true
                }
            }).on('select2:select', (e) => {

            });

            var selOptionRegion = $("<option selected='selected'></option>").val("{{ $data->region->id }}").text("{{ $data->region->name }}")
            $('#selectRegion').append(selOptionRegion).trigger('change.select2');

            var selOptionMobilType = $("<option selected='selected'></option>").val("{{ $data->vehicleType->id }}").text("{{ $data->vehicleType->type_name }}")
            $('#selectMobilType').append(selOptionMobilType).trigger('change.select2');
        })
    </script>
@endpush
