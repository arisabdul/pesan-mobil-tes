@extends('layouts.app')
@section('title', 'Daftar Mobil')
@section('activeMenuMobil', 'active')
@section('activeSubMenuMobilLog', 'active')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header bg-light d-flex justify-content-start align-items-center text-white">
                        <ul class="nav nav-pills nav-fill">
                            <li class="nav-item">
                                <a class="nav-link @yield('activeSubMenuMobil')" aria-current="page" href="{{ route('mobil.mobil.index') }}">List Mobil Type</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link @yield('activeSubMenuMobilType')" href="{{ route('mobil.mobil-type.index') }}">Mobil Type</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link @yield('activeSubMenuMobilLog')" href="{{ route('mobil.mobil-log.index') }}">Riwayat Pemakaian Mobil</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link @yield('activeSubMenuServiceLog')" href="{{ route('mobil.service-log.index') }}">Service Log</a>
                            </li>
                        </ul>
                    </div>

                    <div class="card-body">
                        <div class="w-100 d-flex text-white align-items-center bg-dark p-2">
                            <div class="me-auto ms-2">
                                <h5 class="mb-0">List Mobil</h5>
                            </div>
                        </div>

                        <div class="row my-4">
                            <h5 class="mb-2">Filter Table</h5>
                            <div class="form-group col-md-3 col-sm-12">
                                <label>Type Mobil</label>
                                <select name="id_type" class="form-control" id="selectMobilType" autofocus>

                                </select>
                            </div>
                            <div class="form-group col-md-3 col-sm-12">
                                <label>Ownership</label>
                                <select name="id_mobil" class="form-control" id="selectOwnership" autofocus>
                                    <option value="" selected disabled>Pilih Ownership</option>
                                    <option value="owned">Owned</option>
                                    <option value="leased">Leased</option>
                                </select>
                            </div>
                            <div class="form-group col-md-3 col-sm-12">
                                <label>Mobil</label>
                                <select name="id_mobil" class="form-control" id="selectMobil" autofocus>

                                </select>
                            </div>
                        </div>

                        <div class="table-responsive">
                            <table class="table table-bordered data-table">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Tipe Mobil</th>
                                        <th>Kepemilikan</th>
                                        <th>Plat Nomor</th>
                                        <th>Region</th>
                                        <th>Status</th>
                                        <th width="100px">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('after-script')
    <script>
        $(document).ready(function() {
            var table = $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                bDestroy: true,
                ajax: {
                    tupe: "GET",
                    url: "{{ route('mobil.mobil-log.datatable') }}",
                    data: function(d) {
                        d.mobilType = $('#selectMobilType').val();
                        d.ownership = $('#selectOwnership').val();
                        d.mobil = $('#selectMobil').val();
                    },
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'name',
                        name: 'name'
                    },
                    {
                        data: 'vehicle_type.type_name',
                        name: 'vehicleType.type_name'
                    },
                    {
                        data: 'license_plate',
                        name: 'license_plate'
                    },
                    {
                        data: 'ownership',
                        name: 'ownership'
                    },
                    {
                        data: 'region.name',
                        name: 'region.name'
                    },
                    {
                        data: 'status',
                        name: 'status',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });

            $('#selectMobilType').select2({
                placeholder: 'Pilih Mobil Type',
                theme: 'bootstrap-5',
                width: '100%',
                allowClear: true,
                ajax: {
                    url: "{{ route('select2.select-mobil-type') }}",
                    dataType: 'json',
                    delay: 250,
                    data: function(params) {
                        return {
                            term: params.term || '',
                            page: params.page || 1,
                        }
                    },
                    cache: true
                }
            }).on('select2:select', (e) => {
                table.draw();
            });

            $('#selectOwnership').select2({
                theme: 'bootstrap-5',
                placeholder: 'Pilih Ownership',
                allowClear: true,
            }).on('select2:select', (e) => {
                table.draw();
            });

            $('#selectMobil').select2({
                placeholder: 'Pilih Region',
                theme: 'bootstrap-5',
                width: '100%',
                allowClear: true,
                ajax: {
                    url: "{{ route('select2.select-mobil') }}",
                    dataType: 'json',
                    delay: 250,
                    data: function(params) {
                        return {
                            term: params.term || '',
                            page: params.page || 1,
                            type: $('#selectMobilType').val(),
                        }
                    },
                    cache: true
                }
            }).on('select2:select', (e) => {
                table.draw();
            });

            $(document).on('change', '.select-status', function(e) {
                e.preventDefault();

                let id = $(this).data('id');
                let status = $(this).val();
                let token = $("meta[name='csrf-token']").attr("content");;

                $.ajax({
                    url: "{{ route('mobil.service-log.change-status') }}",
                    method: 'POST',
                    data: {
                        id: id,
                        "_token": token,
                        status: status
                    },
                    beforeSend: function() {
                        swal.fire({
                            title: 'Sedang mengirim ke server, mohon tunggu...',
                            allowEscapeKey: false,
                            allowOutsideClick: false,
                            didOpen: () => {
                                swal.showLoading();
                            }
                        })
                    },
                    success: function(response) {
                        Swal.fire({
                            title: "Berhasil",
                            text: response.message,
                            icon: "success"
                        });
                        table.draw();
                    },
                    error: function(err) {
                        if (err.status == 422) {
                            txtErr = JSON.parse(err.responseText);
                            swal.fire('Gagal', txtErr.message, 'error')
                        } else {
                            swal.fire('Gagal', err.statusText, 'error')
                        }
                        table.draw();
                    },
                });
            })
        })

    </script>
@endpush
