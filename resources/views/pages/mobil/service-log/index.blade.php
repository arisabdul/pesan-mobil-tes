@extends('layouts.app')
@section('title', 'Daftar Mobil')
@section('activeMenuMobil', 'active')
@section('activeSubMenuServiceLog', 'active')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header bg-light d-flex justify-content-start align-items-center text-white">
                        <ul class="nav nav-pills nav-fill">
                            <li class="nav-item">
                                <a class="nav-link @yield('activeSubMenuMobil')" aria-current="page"
                                    href="{{ route('mobil.mobil.index') }}">List Mobil Type</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link @yield('activeSubMenuMobilType')" href="{{ route('mobil.mobil-type.index') }}">Mobil
                                    Type</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link @yield('activeSubMenuMobilLog')" href="{{ route('mobil.mobil-log.index') }}">Riwayat Pemakaian Mobil</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link @yield('activeSubMenuServiceLog')" href="{{ route('mobil.service-log.index') }}">Service
                                    Log</a>
                            </li>
                        </ul>
                    </div>

                    <div class="card-body">
                        <div class="w-100 d-flex text-white align-items-center bg-dark p-2">
                            <div class="me-auto ms-2">
                                <h5 class="mb-0">Service Log</h5>
                            </div>
                            <div class="ms-auto me-2">
                                <a href="{{ route('mobil.service-log.create') }}" class="btn btn-primary"><i
                                        class="bi bi-plus-circle me-2"></i>Tambah Jadwal Service</a>
                            </div>
                        </div>

                        <div class="row my-4">
                            <h5 class="mb-2">Filter Table</h5>
                            <div class="form-group col-md-3 col-sm-12">
                                <label>Type Mobil</label>
                                <select name="id_type" class="form-control" id="selectMobilType" autofocus>

                                </select>
                            </div>
                            <div class="form-group col-md-3 col-sm-12">
                                <label>Ownership</label>
                                <select name="id_mobil" class="form-control" id="selectOwnership" autofocus>
                                    <option value="" selected disabled>Pilih Ownership</option>
                                    <option value="owned">Owned</option>
                                    <option value="leased">Leased</option>
                                </select>
                            </div>
                            <div class="form-group col-md-3 col-sm-12">
                                <label>Mobil</label>
                                <select name="id_mobil" class="form-control" id="selectMobil" autofocus>

                                </select>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered data-table">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Mobil</th>
                                        <th>Tipe Mobil</th>
                                        <th>Kepemilikan</th>
                                        <th>Plat Nomor</th>
                                        <th>Region</th>
                                        <th>Tanggal Perbaikan</th>
                                        <th>Tanggal Selesai</th>
                                        <th>Note Service</th>
                                        <th>Biaya</th>
                                        <th>Status</th>
                                        <th width="100px">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="laporanModal" tabindex="-1" aria-labelledby="laporanModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="POST" id="formLaporan">
                    @csrf
                    @method('PUT')
                    <div class="modal-header">
                        <h5 class="modal-title" id="laporanModalLabel">Laporan Pasca Service</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="preview-laporan table-responsive">

                        </div>
                        <hr>
                        <h4 class="my-4"> Isi Form Laporan</h4>
                        <div class="form-group mb-4">
                            <label for="end_date">Tanggal Selesai</label>
                            <input type="date" name="end_date" class="form-control input-tanggal-selesai">
                        </div>

                        <div class="form-group mb-4">
                            <label for="service_fees">Biaya Service</label>
                            <input type="numeric" name="service_fees" class="form-control input-service-fees" placeholder="Masukkan Biaya Service">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batalkan</button>
                        <input type="submit" class="btn btn-primary" value="Simpan" />
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('after-script')
    <script>
        $(document).ready(function() {
            var table = $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                bDestroy: true,
                ajax: {
                    tupe: "GET",
                    url: "{{ route('mobil.service-log.datatable') }}",
                    data: function(d) {
                        d.mobilType = $('#selectMobilType').val();
                        d.ownership = $('#selectOwnership').val();
                        d.mobil = $('#selectMobil').val();
                    },
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'vehicle.name',
                        name: 'vehicle.name'
                    },
                    {
                        data: 'vehicle.vehicle_type.type_name',
                        name: 'vehicle.vehicleType.type_name'
                    },
                    {
                        data: 'vehicle.ownership',
                        name: 'vehicle.ownership'
                    },
                    {
                        data: 'vehicle.license_plate',
                        name: 'vehicle.license_plate'
                    },
                    {
                        data: 'vehicle.region.name',
                        name: 'vehicle.region.name'
                    },
                    {
                        data: 'start_date',
                        name: 'start_date'
                    },
                    {
                        data: 'end_date',
                        name: 'end_date'
                    },
                    {
                        data: 'note_service',
                        name: 'note_service'
                    },
                    {
                        data: 'service_fees',
                        name: 'service_fees'
                    },
                    {
                        data: 'status',
                        name: 'status',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });

            $('#selectMobilType').select2({
                placeholder: 'Pilih Mobil Type',
                theme: 'bootstrap-5',
                width: '100%',
                allowClear: true,
                ajax: {
                    url: "{{ route('select2.select-mobil-type') }}",
                    dataType: 'json',
                    delay: 250,
                    data: function(params) {
                        return {
                            term: params.term || '',
                            page: params.page || 1,
                        }
                    },
                    cache: true
                }
            }).on('select2:select', (e) => {
                table.draw();
            });

            $('#selectOwnership').select2({
                theme: 'bootstrap-5',
                placeholder: 'Pilih Ownership',
                allowClear: true,
            }).on('select2:select', (e) => {
                table.draw();
            });

            $('#selectMobil').select2({
                placeholder: 'Pilih Region',
                theme: 'bootstrap-5',
                width: '100%',
                allowClear: true,
                ajax: {
                    url: "{{ route('select2.select-mobil') }}",
                    dataType: 'json',
                    delay: 250,
                    data: function(params) {
                        return {
                            term: params.term || '',
                            page: params.page || 1,
                            type: $('#selectMobilType').val(),
                        }
                    },
                    cache: true
                }
            }).on('select2:select', (e) => {
                table.draw();
            });

            $(document).on('change', '.select-status', function(e) {
                e.preventDefault();

                let id = $(this).data('id');
                let status = $(this).val();
                let token = $("meta[name='csrf-token']").attr("content");;

                $.ajax({
                    url: "{{ route('mobil.service-log.change-status') }}",
                    method: 'POST',
                    data: {
                        id: id,
                        "_token": token,
                        status: status
                    },
                    beforeSend: function() {
                        swal.fire({
                            title: 'Sedang mengirim ke server, mohon tunggu...',
                            allowEscapeKey: false,
                            allowOutsideClick: false,
                            didOpen: () => {
                                swal.showLoading();
                            }
                        })
                    },
                    success: function(response) {
                        Swal.fire({
                            title: "Berhasil",
                            text: response.message,
                            icon: "success"
                        });
                        table.draw();
                    },
                    error: function(err) {
                        if (err.status == 422) {
                            txtErr = JSON.parse(err.responseText);
                            swal.fire('Gagal', txtErr.message, 'error')
                        } else {
                            swal.fire('Gagal', err.statusText, 'error')
                        }
                        table.draw();
                    },
                });
            })
        })

        function deleteConf(id) {
            var url = "{{ route('mobil.service-log.destroy', ':id') }}"
            url = url.replace(':id', id)

            Swal.fire({
                icon: 'warning',
                title: 'Apa kamu yakin?',
                text: "Anda tidak akan dapat mengembalikan data ini kembali!",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, hapus!'
            }).then((result) => {
                if (result.value) {
                    window.location.replace(url);
                }
            });
        };

        function setLaporanConf(id) {
            var url = "{{ route('mobil.service-log.show', ':id') }}"
            var urlLaporan = "{{ route('mobil.service-log.set-laporan', ':id') }}"
            url = url.replace(':id', id)
            urlLaporan = urlLaporan.replace(':id', id)
            $('#formLaporan').attr('action', urlLaporan)

            $.ajax({
                url: url,
                method: 'GET',
                success: function(response) {
                    let lp = $('.preview-laporan');
                    let lpDt = '';
                    let dt = response.data;
                    lpDt += `
                        <table class="table table-bordered">
                            <tr>
                                <td>Nama Mobil</td>
                                <td>${dt.vehicle.name}</td>
                            </tr>
                            <tr>
                                <td>Type Mobil</td>
                                <td>${dt.vehicle.vehicle_type.type_name}</td>
                            </tr>
                            <tr>
                                <td>Kepemilikan</td>
                                <td>${dt.vehicle.ownership}</td>
                            </tr>
                            <tr>
                                <td>Plat Nomor</td>
                                <td>${dt.vehicle.license_plate}</td>
                            </tr>
                            <tr>
                                <td>Plat Nomor</td>
                                <td>${dt.vehicle.region.name}</td>
                            </tr>
                            <tr>
                                <td>Tanggal Perbaikan</td>
                                <td>${dt.start_date}</td>
                            </tr>
                            <tr>
                                <td>Note Service</td>
                                <td>${dt.note_service}</td>
                            </tr>
                        </table>
                    `;
                    lp.html(lpDt);
                    $('.input-tanggal-selesai').val(dt.tgl_selesai);
                    $('.input-service-fees').val(dt.service_fees);
                    $('#laporanModal').modal('show')
                },
                error: function(err) {
                    if (err.status == 422) {
                        txtErr = JSON.parse(err.responseText);
                        swal.fire('Gagal', txtErr.message, 'error')
                    } else {
                        swal.fire('Gagal', err.statusText, 'error')
                    }
                    table.draw();
                },
            });
        };
    </script>
@endpush
