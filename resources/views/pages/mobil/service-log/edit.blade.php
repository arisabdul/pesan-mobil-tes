@extends('layouts.app')
@section('title', 'Edit Service Log')
@section('activeMenuMobil', 'active')
@section('activeSubMenuServiceLog', 'active')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header bg-dark text-white">
                        <div class="me-auto">
                            Edit Jadwal Service
                        </div>
                    </div>

                    <div class="card-body">
                        <form action="{{ route('mobil.service-log.update', $data->id) }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="col-md-7 offset-md-3 my-4">
                                <h5>Data Jadwal Service</h5>
                            </div>

                            <div class="row mb-3">
                                <label for="id_type" class="col-md-4 col-form-label text-md-end">Mobil Type</label>

                                <div class="col-md-6">
                                    <select name="id_type" class="form-control" id="selectMobilType" autofocus>

                                    </select>

                                    @error('id_type')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="id_mobil" class="col-md-4 col-form-label text-md-end">Mobil</label>

                                <div class="col-md-6">
                                    <select name="id_mobil" class="form-control" id="selectMobil" autofocus>

                                    </select>

                                    @error('id_mobil')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="note_service" class="col-md-4 col-form-label text-md-end">Note Service</label>

                                <div class="col-md-6">
                                    <textarea id="note_service" type="text" placeholder="Masukkan Note Service"
                                        class="form-control @error('note_service') is-invalid @enderror" name="note_service"
                                         required autocomplete="note_service" autofocus>{{ $data->note_service }}</textarea>

                                    @error('note_service')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="start_date" class="col-md-4 col-form-label text-md-end">Tanggal Perbaikan</label>

                                <div class="col-md-6">
                                    <input type="date" class="form-control" required name="start_date" value="{{ \Carbon\Carbon::parse($data->end_date)->format('Y-m-d') }}">

                                    @error('start_date')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>


                            <div class="row mb-3">
                                <div class="col-md-6 offset-md-4 d-flex justify-content-end">
                                    <input type="submit" value="Simpan" class="btn btn-primary pull-right">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('after-script')
    <script>
        $(document).ready(function() {
            $('#selectMobilType').select2({
                placeholder: 'Pilih Mobil Type',
                theme: 'bootstrap-5',
                width: '100%',
                // allowClear: true,
                ajax: {
                    url: "{{ route('select2.select-mobil-type') }}",
                    dataType: 'json',
                    delay: 250,
                    data: function(params) {
                        return {
                            term: params.term || '',
                            page: params.page || 1,
                        }
                    },
                    cache: true
                }
            }).on('select2:select', (e) => {

            });

            $('#selectMobil').select2({
                placeholder: 'Pilih Region',
                theme: 'bootstrap-5',
                width: '100%',
                // allowClear: true,
                ajax: {
                    url: "{{ route('select2.select-mobil') }}",
                    dataType: 'json',
                    delay: 250,
                    data: function(params) {
                        return {
                            term: params.term || '',
                            page: params.page || 1,
                            type: $('#selectMobilType').val(),
                        }
                    },
                    cache: true
                }
            }).on('select2:select', (e) => {

            });

            var selOptionMobil = $("<option selected='selected'></option>").val("{{ $data->vehicle->id }}").text("{{ $data->vehicle->name }}")
            $('#selectMobil').append(selOptionMobil).trigger('change.select2');
            

            var selOptionMobilType = $("<option selected='selected'></option>").val("{{ $data->vehicle->vehicleType->id }}").text("{{ $data->vehicle->vehicleType->type_name }}")
            $('#selectMobilType').append(selOptionMobilType).trigger('change.select2');
        })
    </script>
@endpush
