@extends('layouts.app')
@section('title', 'Dashboard')
@section('activeMenuDashboard', 'active')

@section('content')
    <div class="container">
        <div class="row justify-content">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h4>Grafik Pemakaian Kendaraan </h4>
                    </div>
                    <div class="card-body">
                        <canvas id="pemakaianKendaraan" width="400" height="160"></canvas>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">
                        <h4>Pemesanan Terbaru</h4>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered">
                            @foreach ($booking as $key => $value)
                                <tr>
                                    <td>{{ $value->employee->name }} <div>{{ $value->vehicle->name }}</div></td>
                                    <td>{{ \App\Models\Booking::STATUS[$value->status] }}</td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('after-script')
    <script>
        // MobilChart
        var dataVehicle = JSON.parse('{!! $vehicle !!}');
        var dtX = [];
        var dtY = [];
        var dtBg = [];
        
        $.each(dataVehicle, function(key, value) {
            dtBg = 'rgba(54, 162, 235, 0.2)';
            if (value.ownership == 'owned')
                dtBg = 'rgba(153, 102, 255, 0.2)';
            
            dtY.push([value.booking_count]);
            dtX.push([value.name]);
        })

        const data = {
            labels: dtX,
            datasets: [{
                label: 'Cars',
                data: dtY,
                backgroundColor: dtBg,
                borderWidth: 1
            }]
        };

        const options = {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        };

        const ctx = document.getElementById('pemakaianKendaraan').getContext('2d');

        const pemakaianKendaraan = new Chart(ctx, {
            type: 'bar',
            data: data,
            options: options
        });
        
    </script>
@endpush
