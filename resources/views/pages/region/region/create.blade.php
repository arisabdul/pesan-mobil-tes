@extends('layouts.app')
@section('title', 'Tambah Region')
@section('activeMenuRegion', 'active')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header bg-dark text-white">
                        <div class="me-auto">
                            Tambah Region
                        </div>
                    </div>

                    <div class="card-body">
                        <form action="{{ route('region.region.store') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="col-md-7 offset-md-3 my-4">
                                <h5>Data Region</h5>
                            </div>

                            <div class="row mb-3">
                                <label for="name" class="col-md-4 col-form-label text-md-end">Nama Region</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" placeholder="Masukkan Nama Lengkap"
                                        class="form-control @error('name') is-invalid @enderror" name="name"
                                        value="{{ old('name') }}" required autocomplete="name" autofocus>

                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                <div class="col-md-6 offset-md-4 d-flex justify-content-end">
                                    <input type="submit" value="Simpan" class="btn btn-primary pull-right">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('after-script')
    <script>
        
    </script>
@endpush
