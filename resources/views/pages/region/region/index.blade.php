@extends('layouts.app')
@section('title', 'List Region')
@section('activeMenuRegion', 'active')
@section('activeSubMenuPegawai', 'active')

@section('content')
    <div class="container">
        <div class="row justify-content-center">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="w-100 d-flex text-white align-items-center bg-dark p-2">
                            <div class="me-auto ms-2">
                                <h5 class="mb-0">List Region</h5>
                            </div>
                            <div class="ms-auto me-2">
                                <a href="{{ route('region.region.create') }}" class="btn btn-primary"><i
                                        class="bi bi-plus-circle me-2"></i>Tambah Region</a>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered data-table">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th width="100px">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('after-script')
    <script>
        $(document).ready(function() {
            var table = $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                bDestroy: true,
                ajax: "{{ route('region.region.datatable') }}",
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'name',
                        name: 'name'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });
        })

        function deleteConf(id) {
            var url = "{{ route('region.region.destroy', ':id') }}"
            url = url.replace(':id', id)

            Swal.fire({
                icon: 'warning',
                title: 'Apa kamu yakin?',
                text: "Anda tidak akan dapat mengembalikan data ini kembali!",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, hapus!'
            }).then((result) => {
                if (result.value) {
                    window.location.replace(url);
                }
            });
        };
    </script>
@endpush
