@extends('layouts.app')
@section('title', 'Pemesanan Mobil')
@section('activeMenuPemesananMobil', 'active')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="w-100 d-md-flex  text-white align-items-center bg-dark p-2">
                            <div class="me-auto ms-2">
                                <p class="mb-0">Pemesanan Mobil {{ $dataRegion->name ?? '-' }}</p>
                            </div>
                            <div class="ms-auto me-2 mt-sm-2 mt-md-0">
                                <a href="{{ route('pemesanan.pemesanan-mobil.export') }}" class="btn btn-success"><i class="bi bi-file-earmark-spreadsheet me-2"></i>Export</a>
                                <a href="{{ route('pemesanan.pemesanan-mobil.create') }}" class="btn btn-primary"><i class="bi bi-plus-circle me-2"></i>Tambah Pemesanan</a>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered data-table">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Driver</th>
                                        <th>Pembuat Pengajuan</th>
                                        <th>Mobil</th>
                                        <th>Plat Nomor</th>
                                        <th>Tipe Mobil</th>
                                        <th>Tanggal Peminjaman</th>
                                        <th>Tanggal Pemgembalian</th>
                                        <th>Approval Booking</th>
                                        <th>Approval Approver</th>
                                        <th>Approval Admin</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('after-script')
    <script>
        $(document).ready(function() {
            var table = $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                bDestroy: true,
                ajax: "{{ route('pemesanan.pemesanan-mobil.datatable') }}",
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'employee.name',
                        name: 'employee.name'
                    },
                    {
                        data: 'pembuat.employee.name',
                        name: 'pembuat.employee.name'
                    },
                    {
                        data: 'vehicle.name',
                        name: 'vehicle.name'
                    },
                    {
                        data: 'vehicle.license_plate',
                        name: 'vehicle.license_plate'
                    },
                    {
                        data: 'vehicle.vehicle_type.type_name',
                        name: 'vehicle.vehicleType.type_name'
                    },
                    {
                        data: 'start_date',
                        name: 'start_date'
                    },
                    {
                        data: 'end_date',
                        name: 'end_date'
                    },
                    {
                        data: 'status_booking',
                        name: 'status_booking'
                    },
                    {
                        data: 'appr_approver',
                        name: 'appr_approver',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'appr_admin',
                        name: 'appr_admin',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });

            $(document).on('change', '.select-status', function(e) {
                e.preventDefault();

                let id = $(this).data('id');
                let status = $(this).val();
                let token = $("meta[name='csrf-token']").attr("content");;

                $.ajax({
                    url: "{{ route('pemesanan.pemesanan-mobil.change-status') }}",
                    method: 'POST',
                    data: {
                        id: id,
                        "_token": token,
                        status: status
                    },
                    beforeSend: function() {
                        swal.fire({
                            title: 'Sedang mengirim ke server, mohon tunggu...',
                            allowEscapeKey: false,
                            allowOutsideClick: false,
                            didOpen: () => {
                                swal.showLoading();
                            }
                        })
                    },
                    success: function(response) {
                        Swal.fire({
                            title: "Berhasil",
                            text: response.message,
                            icon: "success"
                        });
                        table.draw();
                    },
                    error: function(err) {
                        if (err.status == 422) {
                            txtErr = JSON.parse(err.responseText);
                            swal.fire('Gagal', txtErr.message, 'error')
                        } else {
                            swal.fire('Gagal', err.statusText, 'error')
                        }
                        table.draw();
                    },
                });
            })

        })

        function deleteConf(id) {
            var url = "{{ route('pemesanan.pemesanan-mobil.destroy', ':id') }}"
            url = url.replace(':id', id)

            Swal.fire({
                icon: 'warning',
                title: 'Apa kamu yakin?',
                text: "Anda tidak akan dapat mengembalikan data ini kembali!",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, hapus!'
            }).then((result) => {
                if (result.value) {
                    window.location.replace(url);
                }
            });
        };
    </script>
@endpush
