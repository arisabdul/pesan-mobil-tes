<table class="table table-bordered data-table">
    <thead>
        <tr>
            <th>No</th>
            <th>Nama Driver</th>
            <th>Pembuat Pengajuan</th>
            <th>Mobil</th>
            <th>Plat Nomor</th>
            <th>Tipe Mobil</th>
            <th>Tanggal Peminjaman</th>
            <th>Tanggal Pemgembalian</th>
            <th>Tujuan</th>
            <th>Approval Booking</th>
            <th>Approval Approver</th>
            <th>Approval Admin</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($booking as $key => $value)
            <tr>
                <td>{{ $key + 1 }}</td>
                <td>{{ $value->employee->name }}</td>
                <td>{{ $value->pembuat->employee->name }}</td>
                <td>{{ $value->vehicle->name }}</td>
                <td>{{ $value->vehicle->license_plate }}</td>
                <td>{{ $value->vehicle->vehicleType->type_name }}</td>
                <td>{{ $value->start_date }}</td>
                <td>{{ $value->end_date }}</td>
                <td>{{ $value->purpose }}</td>
                <td>{{ \App\Models\Booking::STATUS[$value->status] }}</td>
                <td>{{ !empty($value->approvalApprover->status) ? \App\Models\Approval::STATUS['approver'][$value->approvalApprover->status] : 'Belum direspon approver' }}</td>
                <td>{{ !empty($value->approvalAdmin->status) ? \App\Models\Approval::STATUS['admin'][$value->approvalAdmin->status] : 'Belum direspon admin' }}</td>
            </tr>
        @endforeach
    </tbody>
</table>
