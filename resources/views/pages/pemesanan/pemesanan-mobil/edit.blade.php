@extends('layouts.app')
@section('title', 'Edit Mobil')
@section('activeMenuPemesananMobil', 'active')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header bg-dark text-white">
                        <div class="me-auto">
                            Edit Pemesanan Mobil
                        </div>
                    </div>

                    <div class="card-body">
                        <form action="{{ route('pemesanan.pemesanan-mobil.update', $data->id) }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="col-md-7 offset-md-3 my-4">
                                <h5>Data Pemesanan Mobil</h5>
                            </div>

                            <div class="row mb-3">
                                <label for="id_type" class="col-md-4 col-form-label text-md-end">Mobil Type</label>

                                <div class="col-md-6">
                                    <select name="id_type" class="form-control" id="selectMobilType" autofocus>

                                    </select>

                                    @error('id_type')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="id_mobil" class="col-md-4 col-form-label text-md-end">Mobil</label>

                                <div class="col-md-6">
                                    <select name="id_mobil" class="form-control" id="selectMobil" autofocus>

                                    </select>

                                    @error('id_mobil')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="id_driver" class="col-md-4 col-form-label text-md-end">Driver</label>

                                <div class="col-md-6">
                                    <select name="id_driver" class="form-control" id="selectDriver" autofocus>

                                    </select>

                                    @error('id_driver')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            
                            <div class="row mb-3">
                                <label for="purpose" class="col-md-4 col-form-label text-md-end">Tujuan Pemesanan</label>

                                <div class="col-md-6">
                                    <textarea id="purpose" type="text" placeholder="Masukkan Tujuan Pemesanan"
                                        class="form-control @error('purpose') is-invalid @enderror" name="purpose"
                                         required autocomplete="purpose" autofocus>{{ $data->purpose }}</textarea>

                                    @error('purpose')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="tanggal_peminjaman" class="col-md-4 col-form-label text-md-end">Tanggal Peminjaman</label>

                                <div class="col-md-6">
                                    <input type="date" class="form-control" required name="tanggal_peminjaman" value="{{ \Carbon\Carbon::parse($data->tanggal_peminjaman)->format('Y-m-d') }}">

                                    @error('tanggal_peminjaman')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="tanggal_pengembalian" class="col-md-4 col-form-label text-md-end">Tanggal Pengembalian</label>

                                <div class="col-md-6">
                                    <input type="date" class="form-control" required name="tanggal_pengembalian" value="{{ \Carbon\Carbon::parse($data->tanggal_pengembalian)->format('Y-m-d') }}">

                                    @error('tanggal_pengembalian')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                <div class="col-md-6 offset-md-4 d-flex justify-content-end">
                                    <input type="submit" value="Edit" class="btn btn-primary pull-right">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('after-script')
    <script>
        $(document).ready(function() {
            $('#selectMobilType').select2({
                placeholder: 'Pilih Mobil Type',
                theme: 'bootstrap-5',
                width: '100%',
                // allowClear: true,
                ajax: {
                    url: "{{ route('select2.select-mobil-type') }}",
                    dataType: 'json',
                    delay: 250,
                    data: function(params) {
                        return {
                            term: params.term || '',
                            page: params.page || 1,
                        }
                    },
                    cache: true
                }
            }).on('select2:select', (e) => {

            });

            $('#selectMobil').select2({
                placeholder: 'Pilih Region',
                theme: 'bootstrap-5',
                width: '100%',
                // allowClear: true,
                ajax: {
                    url: "{{ route('select2.select-mobil') }}",
                    dataType: 'json',
                    delay: 250,
                    data: function(params) {
                        return {
                            term: params.term || '',
                            page: params.page || 1,
                            type: $('#selectMobilType').val(),
                        }
                    },
                    cache: true
                }
            }).on('select2:select', (e) => {

            });

            $('#selectDriver').select2({
                placeholder: 'Pilih Driver',
                theme: 'bootstrap-5',
                width: '100%',
                // allowClear: true,
                ajax: {
                    url: "{{ route('select2.select-driver') }}",
                    dataType: 'json',
                    delay: 250,
                    data: function(params) {
                        return {
                            term: params.term || '',
                            page: params.page || 1,
                        }
                    },
                    cache: true
                }
            }).on('select2:select', (e) => {

            });

            var selOptionMobil = $("<option selected='selected'></option>").val("{{ $data->vehicle->id }}").text("{{ $data->vehicle->name }}")
            $('#selectMobil').append(selOptionMobil).trigger('change.select2');
            
            var selOptionDriver = $("<option selected='selected'></option>").val("{{ $data->employee->id }}").text("{{ $data->employee->name }}")
            $('#selectDriver').append(selOptionDriver).trigger('change.select2');

            var selOptionMobilType = $("<option selected='selected'></option>").val("{{ $data->vehicle->vehicleType->id }}").text("{{ $data->vehicle->vehicleType->type_name }}")
            $('#selectMobilType').append(selOptionMobilType).trigger('change.select2');
        })
    </script>
@endpush
