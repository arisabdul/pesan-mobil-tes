<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Select2Controller;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    // return view('welcome');
    return redirect()->route('login');
});

Auth::routes();

// Select2 Controller
Route::group(['prefix' => '/select2', 'as' => 'select2.', 'namespace' => 'App\Http\Controllers'], function () {
    Route::get('/select2/select-region', [
        'uses' => 'Select2Controller@selectRegion',
        'as' => 'select-region'
    ]);
    Route::get('/select2/select-mobil-type', [
        'uses' => 'Select2Controller@selectMobilType',
        'as' => 'select-mobil-type'
    ]);
    Route::get('/select2/select-mobil', [
        'uses' => 'Select2Controller@selectMobil',
        'as' => 'select-mobil'
    ]);
    Route::get('/select2/select-driver', [
        'uses' => 'Select2Controller@selectDriver',
        'as' => 'select-driver'
    ]);
});

// Dashboard
Route::get('/dashboard', [App\Http\Controllers\DashboardController::class, 'index'])->name('dashboard');

Route::group(['prefix' => '/pegawai', 'as' => 'pegawai.', 'namespace' => 'App\Http\Controllers\Pegawai'], function () {
    // List Pegawai
    Route::resource('/pegawai', 'PegawaiController')->except('destroy', 'show');
    Route::get('/pegawai/datatable', [
        'uses' => 'PegawaiController@datatable',
        'as' => 'pegawai.datatable'
    ]);
    Route::post('/pegawai/change-status', [
        'uses' => 'PegawaiController@changeStatus',
        'as' => 'pegawai.change-status'
    ]);
    Route::get('/pegawai/destroy/{id}', [
        'uses' => 'PegawaiController@destroy',
        'as' => 'pegawai.destroy'
    ]);
});

Route::group(['prefix' => '/mobil', 'as' => 'mobil.', 'namespace' => 'App\Http\Controllers\Mobil'], function () {
    // List Mobil
    Route::resource('/mobil', 'MobilController')->except('destroy', 'show');
    Route::get('/mobil/datatable', [
        'uses' => 'MobilController@datatable',
        'as' => 'mobil.datatable'
    ]);
    Route::post('/mobil/change-status', [
        'uses' => 'MobilController@changeStatus',
        'as' => 'mobil.change-status'
    ]);
    Route::get('/mobil/destroy/{id}', [
        'uses' => 'MobilController@destroy',
        'as' => 'mobil.destroy'
    ]);

    // List Mobil
    Route::resource('/mobil-type', 'MobilTypeController')->except('destroy', 'show');
    Route::get('/mobil-type/datatable', [
        'uses' => 'MobilTypeController@datatable',
        'as' => 'mobil-type.datatable'
    ]);
    Route::get('/mobil-type/destroy/{id}', [
        'uses' => 'MobilTypeController@destroy',
        'as' => 'mobil-type.destroy'
    ]);

    // Mobil Log
    Route::get('/mobil-log', [
        'uses' => 'MobilLogController@index',
        'as' => 'mobil-log.index'
    ]);
    Route::get('/mobil-log/datatable', [
        'uses' => 'MobilLogController@datatable',
        'as' => 'mobil-log.datatable'
    ]);

    // Service Log
    Route::resource('/service-log', 'ServiceLogController')->except('destroy', 'show');
    Route::get('/service-log/datatable', [
        'uses' => 'ServiceLogController@datatable',
        'as' => 'service-log.datatable'
    ]);
    Route::put('/service-log/set-laporan/{id}', [
        'uses' => 'ServiceLogController@setLaporan',
        'as' => 'service-log.set-laporan'
    ]);
    Route::post('/service-log/change-status', [
        'uses' => 'ServiceLogController@changeStatus',
        'as' => 'service-log.change-status'
    ]);
    Route::get('/service-log/show/{id}', [
        'uses' => 'ServiceLogController@show',
        'as' => 'service-log.show'
    ]);
    Route::get('/service-log/destroy/{id}', [
        'uses' => 'ServiceLogController@destroy',
        'as' => 'service-log.destroy'
    ]);
});

Route::group(['prefix' => '/region', 'as' => 'region.', 'namespace' => 'App\Http\Controllers\Region'], function () {
    // Region
    Route::resource('/region', 'RegionController')->except('destroy', 'show');
    Route::get('/region/datatable', [
        'uses' => 'RegionController@datatable',
        'as' => 'region.datatable'
    ]);
    Route::get('/region/destroy/{id}', [
        'uses' => 'RegionController@destroy',
        'as' => 'region.destroy'
    ]);
});

Route::group(['prefix' => '/pemesanan', 'as' => 'pemesanan.', 'namespace' => 'App\Http\Controllers\Pemesanan'], function () {
    // Pemesanan Mobil
    Route::resource('/pemesanan-mobil', 'PemesananMobilController')->except('destroy', 'show');
    Route::get('/pemesanan-mobil/datatable', [
        'uses' => 'PemesananMobilController@datatable',
        'as' => 'pemesanan-mobil.datatable'
    ]);
    Route::post('/pemesanan-mobil/change-status', [
        'uses' => 'PemesananMobilController@changeStatus',
        'as' => 'pemesanan-mobil.change-status'
    ]);
    Route::get('/pemesanan-mobil/destroy/{id}', [
        'uses' => 'PemesananMobilController@destroy',
        'as' => 'pemesanan-mobil.destroy'
    ]);
    Route::get('/pemesanan-mobil/export', [
        'uses' => 'PemesananMobilController@export',
        'as' => 'pemesanan-mobil.export'
    ]);
    
});

Route::group(['prefix' => '/riwayat', 'as' => 'riwayat.', 'namespace' => 'App\Http\Controllers\RiwayatPakai'], function () {
    Route::get('/datatable', [
        'uses' => 'RiwayatPakaiController@datatable',
        'as' => 'datatable'
    ]);
    // Route::get('/riwayat-pakai/riwayat/{id}', [
    //     'uses' => 'RiwayatPakaiController@edit',
    //     'as' => 'riwayat-pakai.edit'
    // ]);
});
Route::resource('/riwayat', 'App\Http\Controllers\RiwayatPakai\RiwayatPakaiController')->except('destroy');