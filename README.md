
# Sekawan Media - Vechicle Monitoring

This repository is for the Sekawan Media Backend Developer Test. It contains a vehicle monitoring system implemented using Laravel and PostgreSQL.


## Accounts

### Admin Account
- **Username:** admin
- **Password:** [admin-password]
- **Role:** Administrator (full access)

### Approval Account
- **Username:** approver
- **Password:** [approver-password]
- **Role:** Approver (limited permissions)

## Technical Details

- **Database:** PostgreSQL version 12
- **Framework:** Laravel 8
- **PHP Version:** 7.4

## Installation

Follow these steps to get your development environment running:

1. **Clone the repository**
   
   ```bash
   git clone [repository-url]
   cd [repository-directory]


## Installation

Follow these steps to get your development environment running:

1. **Clone the repository**
   
   ```bash
   git clone [repository-url]
   cd [repository-directory]` 

2.  **Install dependencies**
    
    `composer install` 
    
3.  **Set up the environment**
    
    Copy the `.env.example` file to a new file called `.env` and update the environment settings, including database credentials.
    `cp .env.example .env` 
    
4.  **Generate an application key**
    `php artisan key:generate` 
    
5.  **Run migrations**
    
    This will set up your database schema:
    
    `php artisan migrate` 
    
6.  **Seed the database**

    `php artisan db:seed` 
    
8.  **Serve the application**
    
    Start the Laravel development server:
    `php artisan serve` 
    The server will start running on http://localhost:8000.
    
